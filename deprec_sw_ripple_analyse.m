function [alres,fig]=sw_ripple_analyse(ev,type);

win=5;

% final bits of analysis---------------------------------------------------
%% distribution of values:

allres.len=histc([ev.length],allres.lenbin);
allres.mlen=mean([ev.length]);
allres.stdlen=std([ev.length]);

allres.freq=histc([ev.freq],allres.freqbin);
allres.mfreq=mean([ev.freq]);
allres.stdfreq=std([ev.freq]);

allres.amp=histc([ev.amplitude],allres.ampbin);
allres.mamp=mean([ev.amplitude]);
allres.stdamp=std([ev.amplitude]);


%% ev props over time (10 sec bins)
% disp('Hist')
allres.tsbin=[cts(1):20:cts(end)];
[allres.c_time,allres.ind_time]=histc([ev.tsstart],allres.tsbin);

for i=1:length(allres.tsbin)
    if allres.c_time(i)>0
        allres.amp_time(i)=mean([ev([allres.ind_time]==i).amplitude]);
        allres.len_time(i)=mean([ev([allres.ind_time]==i).length]);
        allres.freq_time(i)=mean([ev([allres.ind_time]==i).freq]);
    else
        allres.amp_time(i)=0;
        allres.len_time(i)=0;
        allres.freq_time(i)=0;
    end
end

 [allres.avc] = moving_average(allres.c_time,win,2);
 [allres.avamp] = moving_average(allres.amp_time,win,2);
 [allres.avlen] = moving_average(allres.len_time,win,2);
 [allres.avfreq] = moving_average(allres.freq_time,win,2);
 
%% wave triggered average of 50 high amp events;
% disp('WTA')
allamps=[ev.amplitude];
alltsabs=[ev.tsabsmax];
[dum,ind]=sort(allamps,'descend');
[allres.sta_s,allres.sta_t,allres.sta_E] = sta(alltsabs(ind(1:50)) ,cscx.f, cts,'n',[],[],[-0.05 0.05],1);



%% plot the results

if opt.plt
    fh=figure(1);
    clf
    set(fh,'name', ' Ripple extraction, summary of results','color','w','PaperPositionMode','auto', 'position',[100 100 600 800])
    subplot(5,2, 1:2)
    plot([0 1],[0 1],'w.');
    set(gca, 'box', 'off', 'xcolor', 'w' , 'ycolor' ,'w','xlim',[0 1])%,...
      %'xtick',[],'ytick',[],'outerposition',proppos);
    xpos=0.01; xpos2=0.5; xpos3=0.8;
    %xpos2=0.5;
    fontnm='FixedWidth';
    text(xpos,1.3, 'Ripples' , 'fontsize',14,'FontName',fontnm)
    text(xpos,1, [ pat fn ext ], 'fontsize',12,'FontName',fontnm)
    text(xpos,0.8, ['Detected: ' num2str(size(ev,2)), ' Ripple events in ' num2str(length(cts)/cFs/60) ' mins' ...
                    ' = ' num2str(size(ev,2)/(length(cts)/cFs)) ' Hz ' ], 'fontsize',10,'FontName',fontnm)
    
    text(xpos,0.6, ['mean length: ' num2str(allres.mlen*1000), ' ms' ], 'fontsize',10,'FontName',fontnm)
    text(xpos2,0.6, ['mean amp: ' num2str(allres.mamp), ' uV' ], 'fontsize',10,'FontName',fontnm)
    text(xpos,0.4, ['mean freq: ' num2str(allres.mfreq), ' Hz' ], 'fontsize',10,'FontName',fontnm)
    %text(xpos,0.8, ['      ' fstr.day ' at ' fstr.time ], 'fontsize',14,'FontName',fntstr)

    h=subplot(5,2,3:4);
    plot((cts-cts(1))/60,csc.raw,'k');
   
    hold on
    plot((cts([ev.indabsmax])-cts(1))/60,csc.raw([ev.indabsmax]),'*r');
    hold off
    axis tight
    ylabel('uV')
    box off
    set(h,'xticklabel',[],'TickDir','out', 'xcolor','w')
    
    
    
    h=subplot(5,2,5:6);
    plot((allres.tsbin-cts(1))/60,zscore(allres.avc),'k'); hold on
    plot((allres.tsbin-cts(1))/60,zscore(allres.avamp),'b');
    plot((allres.tsbin-cts(1))/60,zscore(allres.avlen),'r');
    plot((allres.tsbin-cts(1))/60,zscore(allres.avfreq),'g');
    hold off
    axis tight
    box off
    legend({'# events', 'Amplitude','Lengths','Frequency'},'FontSize',7,...
            'position',[  0.759  0.761        0.171     0.079])  
    xlabel('Time(min)')
    ylabel('z-score')
    set(h,'TickDir','out');
    set(h, 'position',[0.13    0.49  0.775  0.124]);
    
    
    subplot(5,2,7)
    plot(allres.sta_t,allres.sta_s,'r')
    title('Wave triggered average')
    axis([-0.05 0.05 -500 500]);
    box off
    
     
     subplot(5,2,8)
     line([ allres.mfreq allres.mfreq],[ 0 max(allres.freq)+5],'color',[1 0 0] ,'linewidth',2)
     hold on 
     bar(allres.freqbin,allres.freq,'k', 'barwidth',1)
     hold off
     axis([ 120 250 0 max(allres.freq)+5])
     title ('Frequency distribution')
     xlabel('Hz')

    
     subplot(5,2,9)
     line([ allres.mamp allres.mamp],[ 0 max(allres.amp)+5],'color',[1 0 0] ,'linewidth',2)
     hold on 
     bar(allres.ampbin,allres.amp,'k', 'barwidth',1)
     hold off
     axis([ 0 400 0 max(allres.amp)+5])
     title ('Amplitude distribution')
     xlabel('uV')


     subplot(5,2,10)
     line([ allres.mlen allres.mlen],[ 0 max(allres.len)+5],'color',[1 0 0] ,'linewidth',2)
     hold on 
     bar(allres.lenbin,allres.len,'k','barwidth',1)
     hold off
     axis([ 0 0.2 0 max(allres.len)+5])
     title ('Length distribution')
     
     
     print ('-dpdf','-painters' ,[pat fn '_' type '.pdf'])
     disp(['  > created '  pat fn '_' type '.pdf'])
end
