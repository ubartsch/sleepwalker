function [out] = sw_rem_temporary_data(opt)
% function to remove temporary data from data directories

temp_dat_fns={'_bandpass', '_widepass', '_mfigure'}; 

warning off all 
if isfield(opt,'pat')
    fn0= [opt.pat opt.fn  '_' opt.detect '_bandpass.mat']; 
else
    fn0='';
end

fns=dir(opt.pat);
    
for i=1:3
    for j=1:length(fns)
        if ~isempty(fns(j).name)
            if strfind(fns(j).name, temp_dat_fns{i})
                
                ret = input ([ '  delete ' fns(j).name '?  [y/n] >  '],'s');
                if strcmp (ret,'y')
                    delete([opt.pat fns(j).name])
                    out=1;
                else
                    out=0;
                end
            end
        end
    end
end
