function [maxsl,maxsli]=max_slope(cscf,ts,startind,flag)
%simple numerical
if size(cscf,1) > size(cscf,2)
    cscf=cscf';
end

if size(ts,1) > size(ts,2)
    ts=ts';
end

dVdt=diff(cscf)./diff(ts);

switch flag
    case 'down'
        [maxsl ,maxsli]=min(dVdt);
    case 'up'
        [maxsl ,maxsli]=max(dVdt);
end

% %least squares fit?
% E=[ ones(size(ts')); ts'];
% c = E'\cscf; %%bug!!! test separately
% fitx = linspace(ts(1),ts(end));
% fity =[ ones(size(fitx)); fitx]'*c;
% 
% if plt
%     figure(3)
%     plot(ts,cscf,'k');
%     hold on
%     plot(fitx,fity,'r');
%     plot(ts(maxsli),cscf(maxsli),'og')
%     hold off
% end

maxsli=startind+maxsli-1;