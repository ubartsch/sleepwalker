function merge_pdfs(fileNames, outputFile)
% Benjamin Großmann (2023). Merge PDF-Documents (https://www.mathworks.com/matlabcentral/fileexchange/89127-merge-pdf-documents),
% MATLAB Central File Exchange. Retrieved February 7, 2023.
memSet = org.apache.pdfbox.io.MemoryUsageSetting.setupMainMemoryOnly();
merger = org.apache.pdfbox.multipdf.PDFMergerUtility;

cellfun(@(f) merger.addSource(f), fileNames)
merger.setDestinationFileName(outputFile)
merger.mergeDocuments(memSet)
