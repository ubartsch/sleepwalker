function [samples,ts0,Fs,info] = load_csc(filename,mode,par)
% load_csc imports Neuralynx *.ncs files into Matlab
%% examples:
%  [csc,ts,Fs] = load_csc('csc4.Ncs','all');
%  [csc,ts,Fs,info] = load_csc('E:\LA04-03\CSC4.Ncs','ts',[4500e6 6500e6]);
%
%% outputs: 
%       samples: row vector of samples (uVolts)
%       ts0:     row vector of timestamps (sec)
%       Fs:      sampling Frequency (Hz)
%       info:    cell array with header information
% 
%% inputs:
%       filename: string of input file name, may include path
%       mode : string argument indicating mode of csc to use
%                'all' : load all
%                'ts'  : load timestamp range (ts in par)
%                'ind' : load index range  (indices in par)
%                'info': load only header info
%       par (only for modes 'ts', and 'ind'):
%                with ts: pair of timestamps [start_ts end_ts]
%                with end: pair of indices [start_ind end_ind]
%%% U Bartsch, University of Bristol, 2009

% Set the field selection for reading CSC files. 
fieldSelection(1) = 1; % Timestamps
fieldSelection(2) = 1; % Channel number
fieldSelection(3) = 1; % Sample Frequency
fieldSelection(4) = 1; % Number of valid samples
fieldSelection(5) = 1; % Samples (EEG/LFP data)
%return header
extractHeader = 1;

switch mode
    case 'all'
        extractMode = 1; % Extract all data
        [ts,channumber,sampFreq,numvalsamp,samp,Header] =...
                      Nlx2MatCSC(filename,fieldSelection,extractHeader,extractMode);
    case 'ind'
        if ~exist('par','var')
            error ('Please add index range as input variable, eg: load_eeg(filename,mode,[index_start index_stop])!')
        end
        extractMode = 2; %index range
        [ts,channumber,sampFreq,numvalsamp,samp,Header] =...
                      Nlx2MatCSC(filename,fieldSelection,extractHeader,extractMode,par./512);      
    case 'ts'
        if ~exist('par','var')
            error ('Please add timestamp range as input variable, eg: load_eeg(filename,mode,[ts_start ts_stop])!')
        end
        extractMode = 4; % ts range
        [ts,channumber,sampFreq,numvalsamp,samp,Header] =...
                      Nlx2MatCSC(filename,fieldSelection,extractHeader,extractMode,par);
    case 'info'
        extractMode = 1;
        fieldSelection(1) = 0; % Timestamps
        fieldSelection(2) = 1; % Channel number
        fieldSelection(3) = 1; % Sample Frequency
        fieldSelection(4) = 1; % Number of valid samples
        fieldSelection(5) = 0; % Samples (EEG data)
        extractHeader = 1;
        [channumber,sampFreq,numvalsamp,Header] =...
                      Nlx2MatCSC(filename,fieldSelection,extractHeader,extractMode,par);
        ts0=[]; samples=[];
        Fs = sampFreq(1,1);
        info.reclen=length(numvalsamp)*512 /Fs/60;
        info.header=Header;
        
        
        return
    otherwise
        error('Unknown option to load_csc!')
        
     
end
                 
 info.numvalsamp=numvalsamp;
 info.header=Header;
 info.chno=channumber;
 
% check for duplicated timestamps?
% [tsu, m, n] = unique(ts,'first');
% if length(tsu)~=length(ts)
%     ts=tsu;
%     samp=samp(m);
%     n=length(ts)-length(tsu);
%     disp(['Found duplicated timestamps, deleted ' num2str(n) 'timestamps and samples!']);
% end

% Transform the 2-D samples array to an 1-D array
samples=reshape(samp,512*size(samp,2),1);

%Get sampling frequency
Fs = sampFreq(1,1);

% create timestamps for every 512 sample buffer 
ts00=[0:1/Fs:511*1/Fs]*1e6;
ts0=zeros(length(ts),512);
for i=1:length(ts00)
    ts0(1:length(ts),i)=ts+ts00(i);
end
%make it a row vector and convert to seconds!
ts0=reshape(ts0',512*size(samp,2),1)./1e6; 

% ts0=repmat(ts00,length(ts),1);
% ts2=zeros(length(ts),512);
% ts2=repmat(ts',1,512);
% ts2=reshape([ts2+ts0]',512*size(samp,2),1); 

% convert to microvolts
k = strfind(Header,'-ADBitVolts');
for i=1:length(k);
    if ~isempty(k{i})
        k0(i)=k{i};
    end
end
scale=  eval(Header{find(k0)}(13:end));
samples = samples*scale*1e6;

