function [ind]=find_intervals(intervals, events)
% [ind]=find_intervals(intervals, process)
% function to find intervals (eg sleep epochs) 
% that contain events (eg event timestamps) 
%
% inputs :
%     intervals - matrix of pairs defining edges (start/stop) of intervals
%                 eg:    intervals = [ 1 10; 20 25; 50 60];
%     events   - vector of timestamps or else
%                 eg process=[ 0 3 5 15 16 21]
% outputs:
%     ind       - logical index vector of nrows interval 
%                 eg [1 1 0]=find_intervals(periods, process)
% units of time must be conistent

% (c) Ullrich Bartsch, University of Bristol 2014
% ubartsch(at)gmail.com
%% --------------------------------------------------------------------------

ind=false(size(intervals(:,1)));
for i=1:size(intervals,1)
    ind(i)= any (events>intervals(i,1) & events<intervals(i,2) );
end


