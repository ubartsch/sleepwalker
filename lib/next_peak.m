function [ind]=next_peak(csc_part,ind0,flag) 
% next_peak finds the closest local maximum or minimum to the end or
% begining in a csc trace part


switch flag
    case 'maxback'
            [~,k]=findpeaks( csc_part ); %max
            if ~isempty(k)
                ind=ind0-(length(csc_part)-max(k)); %closest local max
            else
                disp('   sw > no max detected, used max value in window')
                [~,indmx]=max(csc_part);
                ind=ind0-(length(csc_part)-indmx);
            end
        
    case 'minback'
            [~,k]=findpeaks(-csc_part); %min

            if ~isempty(k)
               ind=ind0 +max(k)-length(csc_part); %closest local min
            else
             disp ('   sw > no min detected, used min value in window')
             [~,indmn]=min(csc_part);
             ind=ind0 -(length(csc_part)-indmn);
            end
          
    case 'maxfwd'      
            [~,k]=findpeaks( csc_part ); %max
            if ~isempty(k)
                ind=ind0+min(k)-1; %closest local max
            else
                disp('   sw > no max detected, used max value in window')
                [~,indmx]=max(csc_part); 
                %mx2(tmp(j,1)-win1:tmp(j,1)) );
                ind=ind0+indmx-1;
            end
        
    case 'minfwd'     
            [~,k]=findpeaks(-csc_part); %min
            if ~isempty(k)
              ind=ind0 + min(k)-1; %closest local min
            else
              disp ('   sw > no min detected, used min value in window')
              [~,indmn]=min(csc_part);
              ind=ind0 + indmn;
            end

end        
        
        
        