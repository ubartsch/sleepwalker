function [csc,ts,Fs,date_ts] = import_SCORE_occ4_fro4_data(file_name,Fs)
%import_SCORE_occ4_fro4_data e


fid = fopen(file_name);

i=0;
while ~feof(fid)
 
    i = i+1;  
    Data.uat{i,1} = fread(fid,5, '*char');
    
    fseek(fid,4,'cof');    
    Data.uat{i,2} = fread(fid,9, '*char')';    
    Data.uat{i,3} = fread(fid,9, '*char')';
    
    fseek(fid,8,'cof');   
    Data.uat{i,4} = fread(fid,Fs*10,'*uint8');
    
end
date_ts = datenum(strcat(Data.uat(1:end-1,2),Data.uat(1:end-1,3)));
Fs = length(Data.uat{1,4})/10;

%csc = zeros((size(Data.uat,1)-1)*Fs*10,1);
csc = vertcat(Data.uat{:,4});
csc = (double(csc)-128)*-1;

ts = (1/Fs:1/Fs:1/Fs*length(csc))';

end

