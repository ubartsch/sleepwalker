function [maxsl,maxsli]=max_slope(cscf,ts,startind,flag)
%simple numerical slope estimation from selected parts of the wave

% 2 slopes: positive (DOWN>UP) and negative (UP>DOWN) with in each wave
% each slope type has two types
% a type: from superthreshold peak to superthreshold peak
% b type: from superthreshold to subthreshold or vice versa



if size(cscf,1) > size(cscf,2)
    cscf=cscf';
end

if size(ts,1) > size(ts,2)
    ts=ts';
end

dVdt0=diff(cscf)./diff(ts);

[pks,ploc]=findpeaks(dVdt0);
[trghs,tloc]=findpeaks(-cscf);


figure(3); clf;
plot(ts,cscf,'ko');
hold on   
plot(ts(1:end-1),dVdt0/10,'y')

hold off

switch flag
    case 'neg1' 
        %% negative wave
        % 
        [pks,ploc]=findpeaks(-cscf);
        [trghs,tloc]=findpeaks(cscf);
        
        cscpt1=cscf(ploc:tloc);
        tspt1=ts(ploc:tloc);
        figure(3); clf;
        plot(ts,cscf,'ko');
        hold on   
        plot(tspt1(1:end-1),dVdt1/10,'y')
        plot(tspt1,cscpt1,'g.');
        plot(tspt1(maxsli_1),cscpt1(maxsli_1),'+b', 'markersize',20)
       
        plot(tspt2(1:end-1),dVdt2/10,'y')
        plot(tspt2,cscpt2,'g.');
        plot(tspt2(maxsli_2),cscpt2(maxsli_2),'+b', 'markersize',20)
        hold off
        
        
        maxsli=startind+maxsli-1;
        
    case 'pos1' 
        %% positive wave
        
        % slope 1 %UP > DOWN
        [pks,ploc]=findpeaks(-cscf);
        [trghs,tloc]=findpeaks(cscf);
        
        cscpt1=cscf(ploc:tloc);
        tspt1=ts(ploc:tloc);
        
        dVdt1=diff(cscpt1)./diff(tspt1);
        [maxsl_1,maxsli_1]=min(dVdt1);
        
        % slope 2 %DOWN > UP
        cscpt2=cscf(tloc:end);
        tspt2=ts(tloc:end);
        
        dVdt2=diff(cscpt2)./diff(tspt2);
        [maxsl_2,maxsli_2]=max(dVdt2);
        
        figure(3); clf;
        plot(ts,cscf,'ko');
        hold on   
        plot(tspt1(1:end-1),dVdt1/10,'y')
        plot(tspt1,cscpt1,'g.');
        plot(tspt1(maxsli_1),cscpt1(maxsli_1),'+b', 'markersize',20)
       
        plot(tspt2(1:end-1),dVdt2/10,'y')
        plot(tspt2,cscpt2,'g.');
        plot(tspt2(maxsli_2),cscpt2(maxsli_2),'+b', 'markersize',20)
        hold off
        
        
        [maxsl ,maxsli]=max(dVdt);
end

% %least squares fit?

% 
% 
% E=[tspt' ones(size(tspt')) ];
% ac = E \ cscpt'; 
% fitx = linspace(ts(1),ts(end));
% fity =[fitx; ones(size(fitx)); ]'*ac;
% 
% %if plt
% 
%     figure(3); clf;
%     plot(ts,cscf,'ko');
%  
%     
%     hold on   
%     plot(ts(1:end-1),dVdt/10,'y')
%     plot(tspt,cscpt,'g.');
%     plot(fitx,fity,'r');
%     plot(ts(maxsli),cscf(maxsli),'og')
%     hold off
%     
% %end

maxsli=startind+maxsli-1;