
fh=gcf;set(fh,'color','w','paperpositionmode','auto');
lns= findobj(gca,'type','line');
set (lns,'linewidth', 2);

xs= findobj(gco,'type','axes');
set (gca,'linewidth', 2,'FontSize', 12,'FontName', 'Arial','box','off')

txt=findall(gca,'type','text');
set (txt,'FontSize',18,'FontName', 'Arial');
set(gca,'TickDir','out'); 

set(groot,{'DefaultAxesXColor','DefaultAxesYColor','DefaultAxesZColor'},{'k','k','k'})