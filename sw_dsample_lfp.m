function [re_csc,re_cts,reFs2]=sw_dsample_lfp(dat,reFs)
% resample_eeg changes the the sampling rate of a csc trace and generates 
% new timestamps
% Example [re_csc,re_cts,reFs]=resample_eeg(csc,cts,cFs,reFs)
% inputs: csc = csc samples vector as loaded with load_eeg
%         cts = originals timestamps vector
%         cFs = original sampling frequency
%         reFs = desired sampling frequency 


csc= dat.csc;
cts= dat.ts; 
cFs= dat.fs;


m=0;

if reFs>=cFs
    reFs2=cFs;
    re_cts=cts;
    re_csc=csc;
    %disp('  resample_eeg> reFs>=cFs, input unchanged!')
else
    
    if all(size(csc)>1) && size(csc,1)<size(csc,2)
        % matrix! transpose samples to columns 
        m=1;
        csc=csc';
    end
   
    n=floor(cFs/reFs);
    %disp(['  resample_eeg> downsampling by factor' num2str(n) ])
    reFs2=floor(cFs/n);
    re_cts=downsample(cts,n);
    re_csc=downsample(csc,n);

    if m
        % matrix, switch back
        re_csc=re_csc';
    end
    
end  


