% script to test loading edf
% edf is a little different since one data file has many channels:
% always set > optx.load=1; this will always load up the data from the
% original data file.
% loading edf you have to set a channel index: 
% eg > opt.edf_ch=1;
clearvars 
fprintf ('   sw > test edf \n\n'  )

fname='data/human_eeg3.edf';

% general options
opt.signaltype='eeg';
opt.filetype='edf';
opt.parallel=0;
%opt.edf_ch={'C3'};
%opt.ts=[1 100000];
opt.load=1;

opt.edf_ch={'C3','C4'};

%% some delta options
optd=opt;


optd.startind=1;
optd.verbose=0; %If set to 1, manually can see SW detection
optd.verbosePlus=0; %Stores manual decision on each event.

optd.detectlim = 3.5; %This is the detection limit - lower to detect smaller amplitude waves
optd.detect='delta';
optd.filterband = [0.5 4];
optd.noiselim=25;
optd.save=1;
optd.plt=1;
optd.reFs=100;


%[optd, delta, d_stats] = sw_run_delta_hEEG(fname,optd);




%% test spindle detection
opts=opt;
opts.load=1;
opts.detect='spindle';
opts.verbose=0; %If set to 1 to check spindle detection visually

%opts.detectlim = 3; %This is the detection limit (standard deviations of the whole signal) - lower to detect smaller amplitude waves
opts.filterband = [8 16];
opts.widepass=0;
opts.detectlim= 3.5;
opts.startendlim=1.5;
opts.noiselim=20;
opts.save=1;
opts.plt=1;
opts.reFs=100;
opts.minamp=    15;          % min amplitude for spindle events in uV


% run the detection
[opts, spindle, s_stats] = sw_run_spindle_hEEG(fname, opts);