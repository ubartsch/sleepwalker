function [fh,fnprnt]= sw_plot_detection(dat,ev, stats,opt)
% funtion to plot a summary of detection results and save as pdf

%%
if isfield(ev,'auto')
    ev=ev.auto;
end

% Get the length of noise epochs and discard it when computing the total duration
% and the frequency of spindles per seconds
noiseLength=0;
for k=1:length(opt.noise)
    noiseLength=noiseLength+length(opt.noise(k).ind);
end

%% if plotting is enabled in options
if opt.plt
    fh=figure;
    clf;
    set(fh,'name', [opt.detect  ' detection , summary of results'],'color','w','PaperPositionMode','auto', 'position',[100 100 600 800])
    subplot(5,2, 1:2)
    plot([0 1],[0 1],'w.');
    set(gca, 'box', 'off', 'xcolor', 'w' , 'ycolor' ,'w','xlim',[0 1],'ylim',[-300 300])%,...
      %'xtick',[],'ytick',[],'outerposition',proppos);
    xpos=0.01; xpos2=0.5; %xpos3=0.8;
    %xpos2=0.5;
    
    fontnm='FixedWidth';
    %'delta'
    text(xpos,1.4,  opt.detect , 'fontsize',14,'FontName',fontnm,'interpreter','none', 'units','normalized')
    % file name
    if isfield(dat,'info');
        text(xpos,1.2, [ opt.fn opt.ext ], 'fontsize',10,'FontName',fontnm, 'interpreter','none', 'units','normalized')
    end
    if isfield(dat,'ch_list');
        text(xpos2,1.2, [ ' channel: ' dat.ch_list{1} ], 'fontsize',10,'FontName',fontnm, 'interpreter','none', 'units','normalized')
    end
    
   
    text(xpos,1, ['Detected: ' num2str(size(ev,2)) ' ' opt.detect ' events in ' num2str((length(dat.ts)-noiseLength)/dat.fs/60)...
                     ' mins   = ' num2str(size(ev,2)/((length(dat.ts)-noiseLength)/dat.fs)) ' Hz ' ], 'fontsize',10,'FontName',fontnm ,'interpreter','none',  'units','normalized')
    text(xpos,0.8, ['mean duration: ' num2str(stats.mlen*1000), ' ms' ], 'fontsize',10,'FontName',fontnm,'interpreter','none', 'units','normalized')
    text(xpos2,0.8, [' | mean amp: ' num2str(stats.mamp), ' uV' ], 'fontsize',10,'FontName',fontnm,'interpreter','none', 'units','normalized')
    text(xpos,0.6, ['mean freq: ' num2str(stats.mfreq), ' Hz' ], 'fontsize',10,'FontName',fontnm,'interpreter','none', 'units','normalized')
    
    % text(xpos,0.2, ['total length: ' num2str(sum(cat(1,ev.length))), ' s' ], 'fontsize',10,'FontName',fontnm,'interpreter','none', 'units','normalized')
    % text(xpos,0.8, ['      ' fstr.day ' at ' fstr.time ], 'fontsize',14,'FontName',fntstr)
     
    %%
    h1=subplot(5,2,3:4);
    
    %plot((dat.ts-dat.ts(1)),dat.csc,'k');
    hs = plot((dat.ts),dat.csc,'k');
%     set(gca,'XTick',0:100:length(dat.ts));
    hold on
    if ~isempty(opt.noise)
        hn= plot(dat.ts([opt.noise.ind]),dat.csc([opt.noise.ind]),'.','color', [.7 .7 .7]);
    end
    
    
    
 
    if isfield (ev,'tsabsmax')
        kk= find(ismember(dat.ts,[ev.tsabsmax]));
        %          plot( (([ev.tsabsmax])-dat.ts(1)) , dat.csc(kk) ,'.r');
        %Check no duplicates (Julia)
        if length(unique([ev.tsabsmax]))==length([ev.tsabsmax])
            he = plot( (([ev.tsabsmax])) , dat.csc(kk) ,'.r');
        else
            he = plot( ((unique([ev.tsabsmax]))) , dat.csc(kk) ,'.r');
        end
    else
        kk= ismember(dat.ts,[ev.tspeak]);
        %plot( ([ev.tspeak]-dat.ts(1)) , dat.csc(kk) ,'.r');
        he = plot( ([ev.tspeak]) , dat.csc(kk) ,'.r');
    end
    
%     %plot csc of events in blue (Julia)
%     if isfield (ev, 'tsstart') && isfield (ev, 'tsstop') && ~strcmp(opt.signaltype,'eeg')
%         if length(unique([ev.tsstart])) == length(unique([ev.tsstop]))
%             strt = find(ismember(dat.ts, unique([ev.tsstart])));
%             stp = find(ismember(dat.ts, unique([ev.tsstop])));
%             
%             for p = 1:length(strt)
%                 plot((dat.ts(strt(p):stp(p))),dat.csc(strt(p):stp(p)),'b');               
%             end
%         else
%             % temp fix for non-unique start/stop times
%             %[~, ind0] = unique([ev.tsstop]);
%             [~, ind] = unique([ev.tsstop]);
%             %ind = union (ind0,ind1);
%             evstart = [ev.tsstart];
%             evstop = [ev.tsstop];
%               
%             evstart = evstart(ind);
%             evstop = evstop(ind);
%             %ind= find_in_interval([evstart' evstop'], dat.ts);
%             [strt] =find (ismembertol(dat.ts, evstart));
%             [stp] =find (ismembertol(dat.ts, evstop));
%             
%             for p = 1:length(strt)
%                 plot((dat.ts(strt(p):stp(p))), dat.csc(strt(p):stp(p)),'b');
%             end
%             
%         end
%     end
%    
    
    
  
    %line(([[ev.tsabsmax]' [ev.tsabsmax]']'-dat.ts(1))./60, repmat([0 1000],length([ev.tsabsmax]),1)' ,'color','r');
    hold off
    axis tight
    ylabel('uV')
    box off
    set(h1,'xticklabel',[],'TickDir','out', 'xcolor','w')
    
    
    if ~isempty(hn)
            legend([hs hn he],{'raw signal', 'noise', opt.detect },'FontSize',9,...
            'position',[ 0.1370    0.7612    0.2217    0.0790]) 
    else
            legend([hs he],{'raw signal', opt.detect},'FontSize',9,...
            'position',[ 0.1370    0.7612    0.2217    0.0790]) 
    end
    
     %%   
    h2=subplot(5,2,5:6); %% event properties over time
     
    % moving average plot
    plot((stats.tsbin),zscore(stats.avc),'k'); 
    hold on
    plot((stats.tsbin),zscore(stats.avamp),'b');
    plot((stats.tsbin),zscore(stats.avlen),'r');
    plot((stats.tsbin),zscore(stats.avfreq),'g');
    hold off
    axis tight
    box off
    
%     % moving average plot?
%     if isfield (ev,'tsabsmax')
%           plot([ev.tsabsmax]/60, [ev.amplitude] ,'.-k'); 
%     else
%           plot([ev.tspeak]/60, [ev.amplitude] ,'.-k');
%     end
%    
%     hold on
%     
%     plot((stats.tsbin-dat.ts(1))/60,zscore(stats.avamp),'b');
%     plot((stats.tsbin-dat.ts(1))/60,zscore(stats.avlen),'r');
%     plot((stats.tsbin-dat.ts(1))/60,zscore(stats.avfreq),'g');
%     hold off
%     axis tight
%     box off
    
    legend({'# events', 'Amplitude','Lengths','Frequency'},'FontSize',9,...
            'position',[  0.759  0.761        0.171     0.079])  
    xlabel('Time (s)')
    ylabel('z-score')
    set(h2,'TickDir','out');
    set(h2, 'position',[0.13    0.49  0.775  0.124], 'ylim',[-1 6]);
    linkaxes([h1 h2],'x')
    
   
  
    %% wave triggered average
    subplot(5,2,7)
    plot(stats.sta_t,stats.sta_s,'r')
    title('Wave triggered average')
    switch opt.detect
        case 'ripple'
            axis([-0.025 0.025 -100 100]);
        case 'spindle'
            
            ly=min(stats.sta_s)+(min(stats.sta_s)/100*50);
            hy=max(stats.sta_s)+(max(stats.sta_s)/100*50);
            %disp (dat.ch_list)
            if isreal(ly) && isreal(hy)  && ly<0 && hy>0
                    axis([-0.5 0.5  ly hy]);           
            else
                    axis([-0.5 0.5  -opt.maxamp/2 opt.maxamp/2]);
            end
            
        case 'delta'
            ly=min(stats.sta_s)+(min(stats.sta_s)/100*50);
            hy=max(stats.sta_s)+(max(stats.sta_s)/100*50);
            %disp (dat.ch_list)
            if isreal(ly) && isreal(hy)  && ly<0 && hy>0
                    axis([-0.5 0.5  ly hy]);           
            else
                    axis([-0.5 0.5  -opt.maxamp/2 opt.maxamp/2]);
            end
    end
    box off
    
     
     subplot(5,2,8)
 
     hold on 
     bar(stats.freqbin,stats.freq,'k', 'barwidth',1)
     line([ stats.mfreq stats.mfreq],[ 0 max(stats.freq)+5],'color',[1 0 0] ,'linewidth',2)
     hold off  
     axis([ opt.freqbin(1) opt.freqbin(end) 0 max(stats.freq)+5])
     title ('Frequency distribution')
     xlabel('Hz')

    
     subplot(5,2,9)
     hold on 
     bar(stats.ampbin,stats.amp,'k', 'barwidth',1)
     hold off
     line([ stats.mamp stats.mamp],[ 0 max(stats.amp)+5],'color',[1 0 0] ,'linewidth',2)
     
     axis([ opt.ampbin(1) opt.ampbin(end) 0 max(stats.amp)+5])
     title ('Amplitude distribution')
     xlabel('uV')


     subplot(5,2,10)
     hold on 
     bar(stats.lenbin,stats.len,'k','barwidth',1)
     hold off
     line([ stats.mlen stats.mlen],[ 0 max(stats.len)+5],'color',[1 0 0] ,'linewidth',2)
     
     axis([  opt.lenbin(1) opt.lenbin(end) 0 max(stats.len)+5])
     title ('Length distribution')
     
     
     %print ([opt.pat opt.fn '_' opt.detect '.pdf'],'-dpdf','-painters')
     if isfield (opt,'resultsdn')
         dnprnt=opt.resultsdn;
     else
         dnprnt=opt.pat;
     end
     
     if  isfield (dat, 'ch_list')
         fnprnt= [ dnprnt opt.fn '_' dat.ch_list{1} '_' opt.detect ];
         
     else
         fnprnt=[ dnprnt opt.fn '_' opt.detect ];
         
     end
     
     if  isfield (opt, 'suffix')
         fnprnt= [ fnprnt '_' opt.suffix ];
         
     end
     
     
     %savefig ([fnprnt '.fig'])
     
     % save to pdf file for later pdf combination 
     print ([fnprnt '.pdf'],'-dpdf')
     
     disp(['   sw > created '  fnprnt '.pdf'])
     
    
else
    fh=[];
    fnprint=[];
    
end