function [dat] = sw_load_LFP (opt)
% [dat] = detect_load_LFP (opt)
%--------------------------------------------------------------------------
% function to load LFP data 
% add custom load routines as needed
% 
% inputs:  1)   structure opt with fields
%             opt.pat :  path to data file in string
%                         eg opt.pat='C:\data\';
%             opt.fn  :  filename as string 
%                         eg opt.fn='csc1';
%             opt.ext :  extension of file as string
%                         eg opt.ext='ncs';
%         
%         structure opt with fields (for Neuralynx data only):
%             opt.ts   :  pair of timestamps for limiting data load to time range 
%                         eg opt.ts=[1.36e6 2.34e6];
%
%             opt.filetype: string with file type indicator
%                          eg opt.filetype='ncs';
%                      
% outputs: 1) structure with fields
%             dat.csc  :   continuoulsy sampled channel data (signal)
%             dat.ts   :   time stamps for each csc sample
%             dat.fs   :   sampling frequency
%             (optional)
%             dat.info :   meta info on data
%
% (c) Ullrich Bartsch, University of Bristol 2014
% ubartsch(at)gmail.com

%% --------------------------------------------------------------------------
% if ~isfield(opt,'filetype') ||  isempty(opt.filetype)
%     error('   sw_detect_load_LFP  > opt has not field filetype or field is empty!')
% end

% possibiltiy to give whole file path in dn
if ischar(opt)
     dn=opt;opt=[];
    [dn0,fn0,ext0] = fileparts(dn);
    
    if ~isempty(ext0)
        opt.pat=[dn0 filesep];
        opt.fn=fn0;
        opt.ext=ext0;
        opt.ts=[];
    end
  
end
if ~strcmp (opt.pat(end), filesep), opt.pat =[ opt.pat filesep]; end

switch opt.ext 
    
    case '.mat'
        % load sleepwalker dat struct from .mat file
        % pre-prepared data in 
        %format dat.csc(1,:) (mV), dat.ts(1,:) (s)
         load([opt.pat  opt.fn opt.ext],'dat');
        
        %change conversion factors field names etc here:
%         dat.csc=data.values*data.scale*1e6;
%         dat.cts=data.times;
%         dat.fs=1/data.interval;
%         
%         dat.csc=data.csc0';
%         dat.ts=data.cts';
%         dat.fs=data.cFs;
        
        dat.info.fn=[ opt.fn opt.ext];
        dat.info.accessed=datestr(now);
        
        if isfield (opt,'ts') && ~isempty(opt.ts)
            k= dat.ts >= opt.ts(1) & dat.ts <= opt.ts(2);
            dat.csc=dat.csc(k);
            dat.ts=dat.ts(k);
        end
        
        %multichannel?
        
        if isfield (opt, 'ch_sel')
            %select one channel to process?
            
        else
            
            
        end
        
        
            
        
   case '.ncs'
       
        if ~isempty(opt.ts)
           [dat.csc,dat.ts,dat.fs,dat.info] = load_csc([opt.pat  opt.fn opt.ext],'ts',opt.ts);
        else
           [dat.csc,dat.ts,dat.fs,dat.info] = load_csc([opt.pat  opt.fn opt.ext],'all');
        end
        dat.info.fn=[ opt.fn opt.ext];
        dat.info.accessed=datestr(now);
        
        
    case {'.occ4','.fro4'} %Lilly EEG files    
        
        % load datasets
        [dat.csc, dat.ts,dat.fs, dat.info] = import_SCORE_occ4_fro4_data([opt.pat  opt.fn opt.ext],400);
        dat.info.fn=[  opt.fn opt.ext];
        dat.info.accessed=datestr(now);
        
        
        
    case {'.continuous'} % open ephys
        
        %pre downsampling to 500 Hz
        %ds=4;
        ds=1;
        [csc, ts, info] = load_open_ephys_data([opt.pat opt.fn opt.ext]); 
        
        
        dat.csc=downsample(csc',ds);
        dat.ts=downsample(ts',ds);
        dat.fs=info.header.sampleRate/ds;
        
        dat.info.fn=[  opt.fn opt.ext];
        
        dat.info.accessed=datestr(now);
        
        
    case '.edf'  %% rodent edf files 
        % set always load=0 to process original data, because of multiple
        % channels in one file
        opt.load=0;
        
        
        % using edfread by Brett Shoelson
        % https://uk.mathworks.com/matlabcentral/fileexchange/31900-edfread?s_tid=mwa_osa_a
        
        % use targetsignals option to read one channel of choice
        %[...] = edfread(...,'targetSignals',targetSignals)
        
        
        if isfield (opt, 'edf_chno')
             chno=opt.edf_chno;
             disp ( ['    sw_load_LFP > EDF channel number set. Loading channel #' num2str(chno) '!'])
             if isfield (opt, 'edf_chlab')
                  opt.suffix = ['_' opt.edf_chlab];
             else
                 opt.suffix = ['_ch' num2str(chno)];
             end

             [header, dat.csc] = sw_edfread([opt.pat opt.fn opt.ext],'TargetSIgnals',chno);


%         elseif isfield (opt, 'edf_chlab')
%              chlab=opt.edf_chlab;
%              disp ( ['    sw_load_LFP > EDF channel number set. Loading channel ' chlab '!'])
%              opt.suffix = [chlab];
% 
% 
%              [header, dat.csc] = edfread([opt.pat opt.fn opt.ext],'SelectedSignals',chlab);

        else
            disp (   '    sw_load_LFP > No edf channel number set. Loading channel #1!')
            chno=1;
            opt.suffix = ['_ch1'];
            [header, dat.csc] = edfread([opt.pat opt.fn opt.ext],'targetSignals',chno);
        end
        %[hd,csc]=edfread([opt.pat opt.fn opt.ext]);
        
        
        %-------------------------------------------
        %data specific amplitude scaling factor!!
        %e.g. scale =1e3;  

         if isfield (opt, 'signal_scaling')
            scale = opt.signal_scaling;  
         else
            scale=1; 
         end
        disp ( ['    sw_load_LFP > EDF signal scaling = ' num2str(scale) '!'])
        dat.csc=dat.csc.*scale;
        %-------------------------------------------
        
       
        sd= strrep (header.startdate, '.','/');
        st= strrep (header.starttime, '.',':');
        ts0=datenum ([sd ' ' st], 'dd/mm/yy HH:MM:SS'); 
        
        dat.fs=header.samples;
        dat.ts = linspace(0, length(dat.csc)./[dat.fs],length(dat.csc));
        
        dat.info.fn=[  opt.fn opt.ext];
        dat.info.recordingstart=datestr(ts0);
        dat.info.accessed=datestr(now);
        
       
        if isfield(opt,'ts') && ~isempty(opt.ts)
            ind= find_in_interval (opt.ts, dat.ts);
            dat.ts=dat.ts(ind);
            dat.csc=dat.csc(ind);
           
        end
        
        

        
    case '.smr' 
       
        
       %%
       cedMLpath = 'D:\ulli\matlab\CED\CEDS64ML';
       addpath( cedMLpath );
       setenv('CEDS64ML', cedMLpath);
       %%

       % assuming CED lib is installed and added to the Matlab path
       cedlibpath = getenv('CEDS64ML');
       if isempty(cedlibpath)
           error('   sw> Cannot load CED files, please install CED matlab libraries')
       end
       addpath(cedlibpath);
       % load ceds64int.dll
       CEDS64LoadLib( cedlibpath );
    
    
       fhand = CEDS64Open( [opt.pat  opt.fn opt.ext] ,1 );
       maxTimeTicks = CEDS64ChanMaxTime( fhand, 1 ) + 1;
       [ dSecs ] = CEDS64TimeBase( fhand );
       [ iChans ] = CEDS64MaxChan( fhand );
       [ iVersion ] = CEDS64Version( fhand );
       
       sUnits=[];i64Div=[];
       for i=1:iChans
            [~ , sUnits{i,1} ] = CEDS64ChanTitle( fhand ,i);
            [ i64Div(i,1) ] = CEDS64ChanDiv( fhand, i);
       end
       
       
       %[ iRead, fVals, i64Time ] = CEDS64ReadWaveF( fhand, iChan, iN, i64From, i64To, maskh )
       
       % assumin 32 bit signals        
       [ nsamp, vals, time ] = CEDS64ReadWaveF( fhand,8, 1e8, 0, maxTimeTicks );
       %plot(vals)
       % from the CED manual
       % SampleRateInHz = 1.0/(CEDS64ChanDiv(fhand, 1)*CEDS64TimeBase(fhand));
       srate = 1 / (CEDS64ChanDiv(fhand, 8)*CEDS64TimeBase(fhand));
       
%       [ ~,  units ] = CEDS64ChanTitle( fhand, 1 )
        [ ~, dScale ] = CEDS64ChanScale( fhand, 8 );
%       [ ~,  dOffset ] = CEDS64ChanOffset( fhand,1  )
%       rate = CEDS64IdealRate( fhand,1 );
       
       [ maxTimeSecs ] = CEDS64TicksToSecs( fhand, maxTimeTicks );
        
       dat.csc = vals.* 1e4 ;
       dat.fs = srate;
       dat.ts = linspace(0, length(dat.csc)./[dat.fs],length(dat.csc));
       

       dat.info.fn=[ opt.fn opt.ext];
       dat.info.accessed=datestr(now);

       CEDS64CloseAll()
       
       
    case 'plx' % plx data from lilly, dat should have fields with headstage number (hsno) and channel number (chno)

        if isfield(dat,'ts')
            
          [dat.csc, dat.ts,dat.fs] = load_Plx_TBSI_lfp([dat.pat dat.fn dat.ext], dat.ch, dat.hs ,2000,dat.ts(1),dat.ts(2));
          dat.info=[];
        else
           [dat.csc, dat.ts,dat.fs] = load_Plx_TBSI_lfp([dat.pat dat.fn dat.ext], dat.ch, dat.hs ,2000);
            dat.info=[];
            
        end
        
%%     add further custom load functions here
% example:
%             case 'abf'    
%                 https://uk.mathworks.com/matlabcentral/fileexchange/6190-abfload  
%                 [d,si]=abfload([opt.pat  opt.fn opt.ext]);
%                 dat.csc=d;
%                 dat.fs=round(1/si*1e6);
%                 dat.ts=0:si:(length(d)*si)-1;
%
     

end

%% check for correct orientation of data: we want row vectors
s=size(dat.csc);
if s(1)>s(2)
   dat.csc=dat.csc';
end    

s=size(dat.ts);
if s(1)>s(2)
   dat.ts=dat.ts';
end


disp(['    sw_load_LFP > loaded ' dat.info.fn   '!'])
  

