function [ignore,dat]=sw_ignore_data(dat,opt)
% %sleepwalker help---------------------------------------------------------
%  sw_ignore_data to ignore parts of the data (e.g. ignore A,W,R)

%  sleepwalker ignore data i.e. to NOT be include in detections
%
%  inputs: dat = data structure
%          opt.ignore = options structure with field ignore and relevant
%          subfields, e.g. 
%                       opt.ignore.by = epoch;
%                       opt.ignore.type={'A','W','R'};
%
%  outputs:
%  ignore:  struct with indices of ignored data
%
%  examples:
%  [ignore] = sw_ignore_data(dat, opt)  
%
%
%(c) Ullrich Bartsch, University of Surrey, 2022
% ubartsch(at)gmail.com

% -brief changelog-
% 08/2022 first implementation to ignore epochs
% %------------------------------------------------------------------------
%

fprintf('\n   sw > sleepwalker ignore data \n')

if strcmp(varargin{2}, 'off')
     %no noise detection
     fprintf('%s \n', '   sw > all data used! ' )
     dat=varargin{1}; 
     noise.ind=[];
     
else

    

    %% add blocks of data to ignore struct
   
    if isfield(opt, 'ignore')
        
        block0 = zeros( length(dat.csc(1,:),1 ));
        
        switch opt.block.by
            case 'epoch'
                             
                
            case 'index'
                
                nsatval= length(find(noise0)) ;
                disp (['   sw> found '  num2str(nsatval)  '  saturated samples'])
        end
    end 


    %% zscore the broadband signal and create logical mask to exclude high SD data points
    zcsc=zscore((dat.csc).^2);
    noise1 = (zcsc>nthreshold);

    noise2 = noise0 | noise1;


    %% get start and stop indices for noise periods

    %nstarti = find (zcsc(1:end-1)<nthreshold & zcsc(2:end)>nthreshold );
    %nstopi =  find (zcsc(1:end-1)>nthreshold & zcsc(2:end)<nthreshold );
    % 
    % if length(nstarti)>length(nstopi) && nstarti(end) > nstopi (end) && length(zcsc) - nstarti(end) < (dat.fs * 60 *60) 
    %     
    %     nstopi=[nstopi length(zcsc)]; 
    %     
    % end
    nstarti=[];

    % updated to use findpeaks on logical vector, UB 04/05/2020
    [pks,locs,w] = findpeaks(double(noise2));
    for i=1:length(pks)
        nstarti(i) = locs(i);
        nstopi(i)= nstarti(i) + w(i);
    end

    % check in plot
    % figure(100)
    % plot(dat.ts, sqrt (dat.csc.^2) )
    % hold on 
    % 
    % plot(dat.ts(noise2), sqrt( dat.csc(noise2).^2 ),'.r')
    % hold off


    nswin=round(win*dat.fs);

    if ~isempty(nstarti)
    %% check for indices plus noise window when that are out of bounds
        for i=1:length(nstarti)
            tmp1 = nstarti(i) - nswin;
            if tmp1<1
                tmp1=1;
            end
            nstart2(i)=tmp1;

            tmp2=nstopi(i);
            tmp2=tmp2+nswin;
            if tmp2>length(dat.csc)
                tmp2=length(dat.csc);
            end
            nstop2(i)=tmp2;
        end
    %% remove short epochs of noise?
        % dns=diff(nstart2);
        % kdns=find(dns<nswin);
        % nstart2(kdns+1)=[];
        % 
        % dns=diff(nstop2);
        % kdns=find(dns<nswin);
        % nstop2(kdns+1)=[];


        %figure(2)
        %plot(dat.ts,zcsc,'k')
        %plot(dat.ts,dat.csc)
    end

    %%
    if any(noise2)

        %% review noise manually

        if manreview
            % manual review of detected noise


            fh = figure(2);
            clf;
            set(gcf,'position',[ 100        600         523         283], 'name', 'Simple noise rejection')
            plot(dat.ts,zcsc,'k')

            hold on
            plot(dat.ts(noise0),zcsc(noise0),'b.')
            plot(dat.ts(nstart2),zcsc(nstart2),'go')
            plot(dat.ts(nstop2),zcsc(nstop2),'ro')
            hold off
            formatfig
            title('   sw > Press any key to view detected noise epochs!')
            for j=1:length(nstart2)

                set(gca,'xlim', [dat.ts(nstart2(j))-win*2 dat.ts(nstop2(j))+win*2])
                resp{j} = input ( '   sw > include selection as noise [y/n] > ','s');

            end    
            %% remove unwanted noise epochs
            k=strcmpi(resp,'n');
            nstart2(k)=[];nstop2(k)=[];
            close(fh);
        end   


    if ~isempty(nstart2)    
            %% generate final noise struct
            %% clear up redundant noise epochs

            for j=1:length(nstart2)
                noise(j).ind=nstart2(j):nstop2(j);
                noise(j).startind=nstart2(j);
                noise(j).stoptind=nstop2(j);
                noise(j).ts_start=dat.ts(nstart2(j));
                noise(j).ts_stop=dat.ts(nstop2(j));   
            end


            fprintf('   sw > %d noise episodes excluded \n',  length(noise) )
            fh=figure(2);
            set(gcf,'position',[19         734        1100         253], 'name', 'Simple noise rejection')
            plot(dat.ts,dat.csc,'k')
            hold on
            noiseindall=[noise.ind];

            plot(dat.ts(noise2),dat.csc(noise2),'b.')
            plot(dat.ts(noiseindall),dat.csc(noiseindall),'b+')
            plot(dat.ts(nstart2),dat.csc(nstart2),'go')
            plot(dat.ts(nstop2),dat.csc(nstop2),'ro')
            hold off
            formatfig
            close (fh) 

            %% experimental:
            % set noise epochs to zero in signal 
            %dat.csc(noiseindall)=0;
            % not good sets off the filters, ICA , & subtract noise component? ?

    %%        
    %         for j=1:length(nstarti)
    %             title([ ' noise epoch #' num2str(j)] )
    %             set(gca,'xlim', [dat.ts(nstopi(j))-win dat.ts(nstopi(j))+win])
    %             %resp{j} = input ( '    > include selection as noise [y/n] > ','s');
    %             pause(0.2)
    % 
    %         end    
    %         close(figure(1));
        else

            fprintf('   sw > %d noise episodes excluded \n',  0 )

            noise.ind=[];
     end


    else


        fprintf('   sw > no noise detected at current threshold = %d \n', nthreshold )
        noise.ind=[];
    end

    %%
    % for i=1:length(k)
    %     plot(


    
end

