function [opt, rip, stats, fh] = sw_run_ripple_LFP(varargin)
% [rip] = sw_ripple_LFP(dat, opt)
% top level function to perform ripple detection 
% inputs: 
%        dat -  data input which can be either
%               a) string with path/file.dat or
%               b) a data structure with the following fields:
%                   dat.csc, dat.
%        opt - options structure with setttings for ripple analysis
% example: [rip] = sw_run_ripple_LFP(dat, opt)
% (c) Ullrich Bartsch, University of Bristol 2014
% with contributions from Julia Heckenast
% ubartsch(at)gmail.com
%% --------------------------------------------------------------------------
close all

if nargin<1
   eval  ( [ 'help run_ripple_LFP' ]) 
   error ( 'sleepwalker error : no arguments for run_ripple LFP! \n   Check the .m-file header for help!' )
   
elseif nargin<2
    dat=varargin{1}; % data 
    opt=struct;      % run with default options
elseif nargin==2
    dat=varargin{1}; % data
    opt=varargin{2}; % options provided
else
    eval  ( [ 'help run_ripple_LFP' ])
    error ( ' sleepwalker error: wrong number of arguments for run_ripple_LFP! \n   Check the .m-file header for help!')
     
end
            

%% some other settings:
pltwin=0.1;    % size of the plotting window in verbose mode
locthrwin=0.1; %  local threshold window


%% default values
opt0  =     {'detect',   'ripple';    % detect ripples
            'verbose',   0;           % verbose on|off [0|1]; eg: opt.verbose=0;
            'verbosePlus',   0;       % verbose Plus = manual check on|off [0|1]; eg: opt.verbosePlus=0 #==off;
            'textoutput',0;           % print text output to command line during automatic detection
            'startind',   1;          % index of candidate event to start detection at 
            'load',      1;           % load previously filtered/rectified raw data? [0|1]; eg opt.load=1;
            'save',      1;           % save filtered/rectified to speed up repeated analysis? [0|1]; eg opt.load=1;
            'reFs',      1000;        % sampling rate to downsample to;  eg opt.reFs=500;
            'filetype',  'ncs';       % data type  eg opt.filetype='ncs';
            'filterband',[120 250];   % bandpass filter edges eg opt.filterband=[120 250];
            'plt',       1;           % generate summary figure and print pdf [0|1]
            'localwin',  0.2;         % window to find beginning and end of event with local threshold
            'minlen',    0.025;        % min length of ripple in sec
            'maxlen',    0.5;         % max length of ripple in sec
            'mingap',    0.1;         % min gap to seperate ripples
            'minamp',    50;          % min amplitude for ripple events in uV
            'maxamp',    1000;        % max amplitude for ripple events in uV
            'detectlim', 3.5;         % detection limit in signal sd
            'startendlim', 1;       % start/end limit for start/end times in signal sd
            'noiselim',   30;         % exclude signal with zscore(sig^2)>= noiselim  
            'smoothenv', [0 0];       % apply moving_average to power envelope? yes|no nsamples [0|1 10]
            'ts',        [];          % limit data to ts range 
            'lenbin', 0 : 0.005: 0.5; % bins for descriptive stats
            'ampbin', 0:5:500;
            'freqbin' 100:2.5:250; };        

%%  add noise limits to settings
                 
for i=1:length(opt0)
    if ~(isfield(opt ,opt0{i,1}))
        opt.( opt0{i,1} ) = opt0{i,2};
        disp([ '   >  opt.' opt0{i,1} ' not set! Using standard value: ' num2str(opt0{i,2})])
    end
end

%% load data
datfields={'csc','ts','fs','dn','fn'};
if  ischar(dat)
    % string input : load from file
    [opt.pat, opt.fn, opt.ext] = fileparts(dat);
    opt.pat=[opt.pat '\'];
    
    
    [dat] = sw_load_LFP(opt) ;
    
elseif isstruct(dat)
    
    % direct data input: check struct with fields csc, ts, fs
  
    
    dat.dn= [pwd '\'];
    dat.fn='csc';
    
    disp ('  > using working directory to save results!' );
    
    for i=1:length(datfields)
        if ~isfield(dat,datfields{i})
            error( [ '  > dat is missing field ' datfields{i} ])
        else
            if isempty(dat.(datfields{i}))
                error( ['  > dat.' datfields{i} ' is empty!' ])
            end
        end
    end
    
    if ~isfield(opt,'pat')
          
        opt.pat = [pwd '\'];
        opt.fn  = '';
        opt.ext = '';
        disp ('  > using working directory to save results!' )
    end
    
   if ~isempty(opt.ts)
       k=dat.ts>opt.ts(1) & dat.ts<opt.ts(2) ;
       dat.ts=dat.ts(k);
       dat.csc=dat.csc(k);
   end
         
end

%% wide band filtering data
dat= sw_filter_lfp (dat, opt);

%% downsample data
[dat.csc,dat.ts,dat.fs]=sw_dsample_lfp(dat,opt.reFs);


%% basic noise rejection
[opt.noise]=sw_simple_noise_rejection(dat, opt.noiselim, 0);

%% filter, rectify and calculate envelope
% insert custom function here to calc signal envelope
[dat,opt]= sw_get_env(dat,opt);% added as dat.env to dat structure


%% run detection
[rip]=sw_detect_ripple(dat,opt);


[stats]=sw_get_stats(dat,rip,opt);
% basic analysis & figure
%[fig] = sw_analyse(rip);
[fh]= sw_plot_detection(dat,rip, stats,opt);


%% stats on method?:
% cohen's d and related stats from 
% <http://journal.frontiersin.org/researchtopic/sleep-spindles-breaking-the-methodological-wall-2389> 

%close all hidden
end

