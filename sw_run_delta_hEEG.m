function [opt,delta,stats] = sw_run_delta_hEEG(varargin)
% [rip] = run_ripple_LFP(dat, opt)
% top level function to perform ripple detection 
% inputs: 
%        1) dat -  data input which can be either
%               a) a string: dat='data/human_eeg3.edf'
%                   or
%               b) a data structure with the following necessary fields:
%                   fieldnames(dat)> dat.csc, dat.ts, dat.fs
%        2) opt -  options structure with options for detection routine, see
%                 default values below.


%% 09/2019: changed from run_delta_LFP.m to accomodate human multichannel EDF

%% check inputs
fprintf ('   sw > test edf \n\n'  )


if nargin<1
   eval  ( [ 'help run_delta_hEEG' ]) 
   error ( '     sw > sleepwalker error : no arguments for run_delta_hEEG! \n   Check the .m-file header for help!' )
   
elseif nargin<2
    dat=varargin{1};
    opt=struct;
    
elseif nargin==2
    dat=varargin{1};
    opt=varargin{2};
    
else
    eval  ( [ 'help run_delta_hEEG' ]) 
    error ( '    sw > sleepwalker error: wrong number of arguments for run_delta_LFP! \n   Check the .m-file header for help!')  
end




    
    
%% default values
%check options  set to standard values if not set:
opt0  =     {'signaltype',    'eeg';        % data type is human EEG
            'detect',   'delta';      % detect slow wave/delta events
            'parallel'   0;
            'verbose',   0;           % verbose on|off [0|1]; eg: opt.verbose=0;
            'verbosePlus',   0;       % verbosePlus on|off [0|1]; eg: opt.verbose=0;
            'again',     1;           % do data filtering, rectifcation and zscoring again
            'load',      0;           % load previously filtered/rectified raw data? [0|1]; eg opt.load=1;
            'save',      1;           % save filtered/rectified to speed up repeated analysis? [0|1]; eg opt.load=1;
            'reFs',      100;         % sampling rate to downsample to;  eg opt.reFs=500;
            'filetype',  'set';       % data type
            'widepass'  ,0;           %do widepass filtering before? > no by default, try to add to fiel prep!
            'filterband',[0.5 4];     % bandpass filter edges
            'plt',       1;           % generate summary figure and print pdf [0|1]
            'pltwin' ,    1;
            'startind',  1;
            'localwin',  0.5;         % window to find beginning and end of event with local threshold
            'minlen',    0.25;        % min length of slow wave/delta in sec
            'maxlen',    3;           % max length of slow wave/delta in sec
            'mingap',    1;           % min gap to seperate slow wave/delta
            'minamp',    25;          % min amplitude for slow wave/delta events in uV
            'maxamp',    500;         % max amplitude for slow wave/delta events in uV
            'detectlim', 3.0;         % detection limit in signal sd
            'startendlim', 1;         % start/end limit for start/end times in signal sd
            'noiselim',   25;         % exclude all signal with sd >= noiselim 
            'noisewin', 1;            % window to exclude around noise detection in s 
            'clipping',  [inf];       % raw uV value that indicates clipping ( = max range )
            'smoothenv', [0 0];       % apply moving_average to power envelope? yes|no nsamples [0|1 10]
            'ts',        [];          % limit data to ts range 
            'lenbin', 0:0.1:3;
            'ampbin', 0:5:500;
            'freqbin', [0:0.2:5]};        
                 
for i=1:length(opt0)
    if ~(isfield(opt ,opt0{i,1}))
        opt.( opt0{i,1} ) = opt0{i,2};
        disp([ '    sw > field opt.' opt0{i,1} ' not set! Using standard value: ' num2str(opt0{i,2})])
    end
end
% some other settings:
pltwin=0.2;    % size of the plotting window in verbose mode
locthrwin=0.1; %  local threshold window



%% load data

%% here we load all edf channels and process them in sequence, unless edf_ch is set:

if  ischar(dat)
    % string input : load from file
    [opt.pat, opt.fn, opt.ext] = fileparts(dat);
    opt.pat=[opt.pat '\'];
  
    [dat] = sw_load_EEG(opt) ;    
else 
    % direct data input: check struct with fields csc, ts, fs
    for i=1:length(datfields)
        if ~isfield(dat,datfields{i})
            error( [ '    sw >  dat has missing field ' datfields{i} ])
        else
            if isempty(dat.(datfields{i}))
                error( ['  > dat.' datfields{i} ' is empty!' ])
            end
        end
    end
    
    if ~isfield(opt,'pat')
          
        opt.pat = [pwd '\'];
        opt.fn  = '';
        opt.ext = '';
        disp ('    sw >  using working directory to save results!' )
    end
    
end

%% save results to other directory than source
if isfield(opt, 'resultsdn')  && isfield(opt, 'id')
    if ~exist(opt.resultsdn)
       mkdir(opt.resultsdn)
    end
    svdn=opt.resultsdn;
else
    svdn=opt.pat;
end

if opt.widepass
    %% wide bandpass filtering : 0.5- 30 Hz
    dat= sw_filter_lfp (dat,opt);
end

%% downsample data
[dat.csc,dat.ts,dat.fs]=sw_dsample_lfp(dat,opt.reFs);
%datall=dat;

manualnoisecheck=0;


if opt.parallel==0;
    
    for i=1:size(dat.csc,1)
        
        %Build single dat struct to run detection:
        dat0=dat;
        dat0.csc=dat.csc(i,:); 
        dat0.ch_list=dat.ch_list(i,:);
       

        %% simple noise rejection
      
        % noisewin = 1;
        % [opt.noise]=sw_simple_noise_rejection(dat,opt.noiselim, 0);
        
        [opt.noise,dat0]=sw_simple_noise_rejection(dat0,opt.noiselim, manualnoisecheck,opt.noisewin, opt.clipping);

        %% filter & envelope
        [dat0,opt]= sw_get_env(dat0,opt);


        %% run detections
        [delta0{i}]=sw_detect_delta(dat0,opt);

        %% basic analysis 
        [stats0{i}]=sw_get_stats(dat0,delta0{i},opt);

        if opt.plt
        %% mk summary figure
            [fh, fnchpdf{i}]= sw_plot_detection(dat0,delta0{i},stats0{i},opt);
        else
            fh=[];
        end

    %     if i==1
    %          print ([opt.pat opt.fn '_' opt.detect '.ps'], '-dpsc2')    
    %     else
    %          print ([opt.pat opt.fn '_' opt.detect '.ps'], '-dpsc2','-append')
    %     end 

        %save results
    end
   
elseif opt.parallel==1
    
    
    pj = gcp('nocreate');
    if isempty(pj)
        if isfield(opt, 'numparworkers')
             pj = parpool('local', opt.numparworkers);
        else
             nc=feature('numcores');
             pj = parpool('local', nc/2);    
        end
    end
    
    %dat.ch_list = dat.ch_list(1:4);
    delta0=cell(length(dat.ch_list),1);
    stats0=cell(length(dat.ch_list),1);
    parfor i=1:length(dat.ch_list)


        %Build single channel dat struct to run detection:
        dat0=dat;
        dat0.csc=dat.csc(i,:); 
        dat0.ch_list=dat.ch_list(i,:);
        opt0=opt; 
      


        %% simple noise rejection

        % [opt.noise]=sw_simple_noise_rejection(dat,opt.noiselim, 0);
        [opt0.noise,dat0]=sw_simple_noise_rejection(dat0,opt0.noiselim, manualnoisecheck);

        %% filter & envelope
        [dat0,opt0]= sw_get_env(dat0,opt0);


        %% run detections
        try 
           [delta0{i}]=sw_detect_delta(dat0,opt0);
        catch me
           delta0{i}=me;
        end
        
        
        if isstruct (delta0{i})
            %% descriptive stats 
            [stats0{i}]=sw_get_stats(dat0,delta0{i},opt0);

            if opt.plt
            %% mk summary figure
                [fh, fnchpdf{i}]= sw_plot_detection(dat0,delta0{i},stats0{i},opt0);
            else
                fh=[];
            end
  
        else
            stats0{i}=struct;
        end
        
        
    end
    %pause(20)
    
    
end

%% collect results into structure for easier readability
for i=1:length(delta0)
    
    delta.(matlab.lang.makeValidName (dat.ch_list{i}) )=delta0{i};
    stats.(matlab.lang.makeValidName (dat.ch_list{i}))=stats0{i};
    
end




if  isfield(opt, 'id')      
    svfn=[svdn opt.id '_'  opt.detect  '_results.mat'];
else
    svfn=[svdn opt.fn '_'  opt.detect  '_results.mat'];
end

save(svfn , opt.detect,'opt','stats')
fprintf ('    sw >  saved  %s \n\n' , svfn )

% create final pdf with all results!
if length(fnchpdf)>1
    fnpdf=[svdn opt.fn '_' opt.detect  '_all_results.pdf'];
    for i=1:length(fnchpdf)
        fnchpdf{i}=[fnchpdf{i} '.pdf'];
    end
    %append_pdfs (fnpdf,fnchpdf);   
    merge_pdfs (sort(fnchpdf),fnpdf);
end
for i=1:length(fnchpdf); delete(fnchpdf{i}) ; end







