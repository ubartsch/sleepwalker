function [stats]=sw_get_stats(dat,ev,opt)
%[stats]=sw_get_stats(dat,ev,opt)

if isfield (ev, 'auto')
    ev=ev.auto;
end
    

histwin=10;% histogram window for averaging event properties in s

%% some descriptive stats---------------------------------------------------
% histogram, mean, std, var of properties 
stats.type=opt.detect;

stats.lenbin=opt.lenbin;
stats.len=histc([ev.length],opt.lenbin);
stats.mlen=nanmean([ev.length]);
stats.stdlen=nanstd([ev.length]);

stats.freqbin=opt.freqbin;
stats.freq=histc([ev.freq],opt.freqbin);
stats.mfreq=nanmean([ev.freq]);
stats.stdfreq=nanstd([ev.freq]);

stats.ampbin=opt.ampbin;
stats.amp=histc([ev.amplitude],opt.ampbin);
stats.mamp=nanmean([ev.amplitude]);
stats.stdamp=nanstd([ev.amplitude]);


%% event dynamics over time (10 sec bins)
%disp('Hist')

stats.tsbin=[dat.ts(1):histwin:dat.ts(end)];
[stats.c_time,stats.ind_time]=histc([ev.tsstart],stats.tsbin);

for i=1:length(stats.tsbin)
    if stats.c_time(i)>0
        stats.amp_time(i)=nanmean([ev([stats.ind_time]==i).amplitude]);
        stats.len_time(i)=nanmean([ev([stats.ind_time]==i).length]);
        stats.freq_time(i)=nanmean([ev([stats.ind_time]==i).freq]);
    else
        stats.amp_time(i)=0;
        stats.len_time(i)=0;
        stats.freq_time(i)=0;
    end
end
win=5;
 [stats.avc] = moving_average(stats.c_time,win,2);
 [stats.avamp] = moving_average(stats.amp_time,win,2);
 [stats.avlen] = moving_average(stats.len_time,win,2);
 [stats.avfreq] = moving_average(stats.freq_time,win,2);
 
 
%% wave triggered average of 50 high amp events;
%disp('STA')
switch opt.detect
    case 'delta'
        istype=[ev.type]==-1;
        allamps=[ev(istype).amplitude];
        alltsabs=[ev(istype).tsabsmax];
        
    otherwise
        allamps=[ev.amplitude];
        alltsabs=[ev.tsabsmax];
end

[~,ind]=sort(allamps,'descend');
if length(allamps)>50, n=50; else n=length(allamps); end

switch opt.detect
    case 'ripple'
        twin=[-0.025 0.025];
    case 'spindle'
         twin=[-0.5 0.5];
    case 'delta'
        twin=[-0.5 0.5];
end

[stats.sta_s,stats.sta_t,stats.sta_E] = sta(alltsabs(ind(1:n)) ,dat.f, dat.ts,'n',[],[],twin,1);

%Find total time of data (- noisy bits) *Added by Julia 30.11.16
noiseLength=0;
for k=1:length(opt.noise)
    noiseLength=noiseLength+length(opt.noise(k).ind);
end
stats.TimeinMins = num2str((length(dat.ts)-noiseLength)/dat.fs/60);

%Save stats
%save ([opt.pat opt.fn '_' opt.detect '_stats.mat'], 'ev' , 'opt', 'stats');
% 
% if  isfield (dat, 'ch_list')
%      fnsv= [opt.pat opt.fn '_' dat.ch_list{1} '_' opt.detect  '_stats.mat'];
%      save(fnsv, 'ev' , 'opt', 'stats');
% 
%  else
%      fnsv=[opt.pat opt.fn '_' opt.detect '_stats.mat'];
%      save (fnsv,'ev' , 'opt', 'stats')
%  end
% 
%  disp(['  > created '  fnsv])

