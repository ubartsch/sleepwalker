% script to test loading edf
% edf is a little different since one data file has many channels:
% always set > optx.load=1; this will always load up the data from the
% original data file.
% loading edf you have to set a channel index: 
% eg > opt.edf_ch=1;
clearvars 
close all hidden 

fprintf ('   sw > test edf \n\n'  )


fname='C:\_data\22q_h\60ch_sleep\data\E004-2-1-1\E004-2-1-1_EEG_60.set';


% general options
opt.signaltype='eeg';
opt.filetype='set';
opt.save=0;
opt.parallel=0;
opt.numparworkers=6;
opt.verbose=0;


%opt.ts=[1 100000];
opt.load=0;
opt.save=0;

opt.edf_ch={'FC2'};
%opt.edf_ch={'C3'};

%% some delta options
optd=opt;


optd.startind=1200;

optd.verbosePlus=0; %Stores manual decision on each event.
optd.mingap=0.75;
optd.detectlim = 3.5; %This is the detection limit - lower to detect smaller amplitude waves
optd.detect='delta';
optd.filterband = [0.5 4];
optd.noiselim=25;
optd.plt=1;
%optd.reFs=100;


[optd, delta, d_stats] = sw_run_delta_hEEG(fname,optd);




%% test spindle detection
opts=opt;

opts.detect='spindle';
opts.verbose=0; %If set to 1 to check spindle detection visually

%opts.detectlim = 3; %This is the detection limit (standard deviations of the whole signal) - lower to detect smaller amplitude waves
opts.filterband = [8 16];
opts.noiselim=25;

opts.plt=1;
%opts.reFs=100;
opts.minamp= 15;   % min amplitude for spindle events in uV


% run the detection
[opts, spindle, s_stats] = sw_run_spindle_hEEG(fname, opts);