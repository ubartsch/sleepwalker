function [noise,dat]=sw_simple_noise_rejection(varargin)
% %sleepwalker help---------------------------------------------------------
% sw_simple_noise rejection accepts different input argument combinations

%  sleepwalker simple noise rejection
%  detect extreme values  in signal based on squared - zscored signal
% case 1  switch off

%  inputs: dat = data structure
%           nthreshold = noise threshold (as squared
%           view = flag for reviewing detected noise manually [0/1]
%  example: [noise]=sw_simple_noise_rejection(dat, nthreshold, view)  
%  [noise,dat]=sw_simple_noise_rejection(dat,opt.noiselim, view, manualnoisecheck, opt.clipping);
%  outputs:
%          noise  =  noise struct with indices, start, end tiems for noise
%                   epochs
%
%(c) Ullrich Bartsch, University of Bristol 2014
% ubartsch(at)gmail.com

% -brief changelog-
% added saturation detection: 05/05/2020
% added windows around putative noise events 12/10/16, UB
% added off option and default values 14/05/18, UB

% %------------------------------------------------------------------------
%default noise threshold
nthreshold = 25; % 25 SD of the input signal

%default time window to exclude around each noise detection 
nswin= 1; % in seconds


fprintf('\n   sw > sleepwalker noise rejection \n')
if strcmp(varargin{2}, 'off')
         fprintf('%s \n', '   sw > noise rejection turned off! ' )
         dat=varargin{1}; 
         noise.ind=[];
else

    
 
if nargin<1
   eval  ( [ 'help sw_simple_noise_rejection' ]) 
   error ( '\n   sw > sleepwalker error  \n  Not enough inputs! \n Check the .m-file header for help!' )
   
elseif nargin==1
    dat=varargin{1};% data structure provided
    manreview=0;         % set no manual review
    nswin=1*dat.fs;
    fprintf('   sw > using standard values: threshold = %g,  no manual review. \n', nthreshold) 

    
elseif nargin==2
    dat=varargin{1}; % data structure
    nthreshold=varargin{2}; % threshold provided
    fprintf('   sw > using custom value: threshold = %g,  no manual review. \n', nthreshold) 
    manreview=0;         % set no manual review
    nswin=1*dat.fs;
    
elseif nargin==3
    dat=varargin{1}; % data structure
    nthreshold=varargin{2}; % threshold provided
    manreview=varargin{3}; 
    nswin=1*dat.fs;
    fprintf('   sw > using custom value: threshold= %g,  manual review=  %d \n', nthreshold, manreview) 
    
elseif nargin==4
    
        dat=varargin{1}; % data structure
        nthreshold=varargin{2}; % threshold provided
        manreview=varargin{3}; 
        nswin=varargin{4}*dat.fs;
        fprintf('   sw > using custom value: threshold= %g, noise window= %g,  manual review=  %d \n', nthreshold, manreview, nswin) 

elseif nargin==5

        dat=varargin{1}; % data structure
        nthreshold=varargin{2}; % threshold provided
        manreview=varargin{3}; 
        nswin=varargin{4}*dat.fs;
        satval = varargin{5}; 
        fprintf('   sw > using custom value: threshold= %g, noise window= %g,  manual review=  %d , saturated value = %g \n', nthreshold, manreview, nswin, satval) 
   
elseif nargin==6

        dat=varargin{1}; % data structure
        nthreshold=varargin{2}; % threshold provided
        manreview=varargin{3}; 
        nswin=varargin{4}*dat.fs;
        satval = varargin{5}; 
        exclude= varargin{6}; 
        fprintf('   sw > using custom value: threshold= %g, noise window= %g,  manual review=  %d , saturated value = %g' , ...
                    nthreshold, nswin, manreview, satval ) 
   

else
    eval  ( [ 'help simple_noise_rejection.m' ])
    error ( '   sw > sleepwalker error: too many arguments for simple_noise_rejection.m! \n   Check the file header for help!')
     
end

%% 

noise0=zeros(size(dat.csc));
%% add saturated signal as noise if option if selected
%satval=499; testing
if exist('satval') & ~isempty(satval)
    noise0 = (dat.csc>=satval) | (dat.csc<=-satval);
end 

%% add noise based on time points 
% noise based on preselected time ranges

noisex =zeros(size(dat.csc));

if exist('exclude')
   
   for i=1:size(exclude,1) 
       if ~isnan(exclude(i,1))
         tmpx = dat.ts>=exclude(i,1) & dat.ts<=exclude(i,2);
         noisex (tmpx) = true; 
       end
   end
    
end
    
    
end
%length(find(noise0))    

    
    



%% zscore the broadband signal and create logical mask to exclude high SD data points
zcsc=zscore((dat.csc).^2);
noise1 = (zcsc>nthreshold);

noise2 = noise0 | noise1 | noisex;
noise2(1) =0;
noise2(end) =0;

%% get start and stop indices for noise periods

%nstarti = find (zcsc(1:end-1)<nthreshold & zcsc(2:end)>nthreshold );
%nstopi =  find (zcsc(1:end-1)>nthreshold & zcsc(2:end)<nthreshold );
% 
% if length(nstarti)>length(nstopi) && nstarti(end) > nstopi (end) && length(zcsc) - nstarti(end) < (dat.fs * 60 *60) 
%     
%     nstopi=[nstopi length(zcsc)]; 
%     
% end
nstarti=[];

% updated to use findpeaks in logical vector, UB 04/05/2020
[pks,locs,w] = findpeaks(double(noise2));

for i=1:length(pks)
    nstarti(i) = locs(i) - w(i);
    nstopi(i)= locs(i) + w(i);
end
   
% check in plot
figure(100)
plot(dat.ts,zcsc ,'.k')
hold on 

plot(dat.ts(noise2), zcsc (noise2),'or')
hold off


%nswin=round(win*dat.fs);

if ~isempty(nstarti)
%% check for indices plus noise window that are out of bounds
    for i=1:length(nstarti)
        tmp1 = nstarti(i) - nswin;
        if tmp1<1
            tmp1=1;
        end
        nstart2(i)=round(tmp1);

        tmp2=nstopi(i);
        tmp2=tmp2+nswin;
        if tmp2>length(dat.csc)
            tmp2=length(dat.csc);
        end
        nstop2(i)=round(tmp2);
    end
%% remove short epochs of noise?
    % dns=diff(nstart2);
    % kdns=find(dns<nswin);
    % nstart2(kdns+1)=[];
    % 
    % dns=diff(nstop2);
    % kdns=find(dns<nswin);
    % nstop2(kdns+1)=[];


    %figure(2)
    %plot(dat.ts,zcsc,'k')
    %plot(dat.ts,dat.csc)
end

%%
if any(noise2)

    %% review noise manually

    if manreview
        % manual review of detected noise
        

        fh = figure(2);
        clf;
        set(gcf,'position',[ 100        600         523         283], 'name', 'Simple noise rejection')
        plot(dat.ts,zcsc,'k')
        
        hold on
        plot(dat.ts(noise2),zcsc(noise2),'b.')
        plot(dat.ts(nstart2),zcsc(nstart2),'go')
        plot(dat.ts(nstop2),zcsc(nstop2),'ro')
        hold off
        formatfig
        title('   sw > Press any key to view detected noise epochs!')
        for j=1:length(nstart2)

            set(gca,'xlim', [dat.ts(nstart2(j))-nswin*2 dat.ts(nstop2(j))+nswin*2])
            resp{j} = input ( '   sw > include selection as noise [y/n] > ','s');

        end    
        %% remove unwanted noise epochs
        k=strcmpi(resp,'n');
        nstart2(k)=[];nstop2(k)=[];
        close(fh);
    end   
    
   
if ~isempty(nstart2)    
        %% generate final noise struct
        %% clear up redundant noise epochs
        
        [nstart2,uind] = unique(nstart2);
        nstop2= nstop2(uind);
        
        for j=1:length(nstart2)
            noise(j).ind=nstart2(j) :  nstop2(j);
            noise(j).startind=nstart2(j) ;
            noise(j).stopind=nstop2(j);
            noise(j).ts_start=dat.ts( nstart2(j) );
            noise(j).ts_stop=dat.ts( nstop2(j) );   
        end
        
        j=1;
        while j<=length(noise)-1
            if noise(j+1).startind < noise(j).stopind
                noise(j).stopind = noise(j+1).startind;
                noise(j+1)=[];
            end
            j=j+1;
        end
                
        


        fprintf('   sw > %d noise episodes excluded \n',  length(noise) )
        fh=figure(2);
        set(gcf,'position',[19         734        1100         253], 'name', 'Simple noise rejection')
        plot(dat.ts,dat.csc,'k')
        hold on
        noiseindall=[noise.ind];
        
        %plot(dat.ts,dat.csc,'b.')
        plot(dat.ts(noiseindall),dat.csc(noiseindall),'bo')
        plot(dat.ts(nstart2),dat.csc(nstart2),'go')
        plot(dat.ts(nstop2),dat.csc(nstop2),'ro')
        hold off
        formatfig
        pause(1)
        close (fh) 
        
        %% experimental:
        % set noise epochs to zero in signal 
        %dat.csc(noiseindall)=0;
        % not good sets off the filters, SSA & subtract noise components?
        
%%        
%         for j=1:length(nstarti)
%             title([ ' noise epoch #' num2str(j)] )
%             set(gca,'xlim', [dat.ts(nstopi(j))-win dat.ts(nstopi(j))+win])
%             %resp{j} = input ( '    > include selection as noise [y/n] > ','s');
%             pause(0.2)
% 
%         end    
%         close(figure(1));
    else
        
        fprintf('   sw > %d noise episodes excluded \n',  0 )
        
        noise.ind=[];
 end
    
    
else
    
    
    fprintf('   sw > no noise detected at current threshold = %d \n', nthreshold )
    noise.ind=[];
end

%%
% for i=1:length(k)
%     plot(


    
end

