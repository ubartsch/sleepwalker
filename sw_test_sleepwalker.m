% script to test sleepwalker functions
  

%% data file location:
path_file='data/csc_PrL_rest.ncs';

% global settings

opt.signaltype=      'lfp';         %detect sleep events in lfp;
opt.noisewin =1;
%% delta detection
% settings
optd=[];
optd=opt;

optd.detect=   'delta';      % detect slow wave/delta events
optd.verbose=   0;           % verbose on|off [0|1]; eg: opt.verbose=0;
optd.again=     0;           % do data filtering, rectifcation and zscoring again
optd.load=      1;           % load previously filtered/rectified raw data? [0|1]; eg opt.load=1;
optd.save=      1;           % save filtered/rectified to speed up repeated analysis? [0|1]; eg opt.load=1;
optd.reFs=      100;         % sampling rate to downsample to;  eg opt.reFs=500;
optd.filetype=  'ncs';       % data type
optd.filterband=[0.5 4];     % bandpass filter edges
optd.plt=       1;           % generate summary figure and print pdf [0|1]
optd.pltwin=    1;           % window to plet detections in verbose mode
optd.startind=  1;           % start index (only for debugging!) 
optd.localwin=  0.5;         % window to find beginning and end of event with local threshold
optd.minlen=    0.25;        % min length of slow wave/delta in sec
optd.maxlen=    3;           % max length of slow wave/delta in sec
optd.mingap=    0.5;         % min time gap to seperate slow wave/delta events
optd.minamp=    50;          % min amplitude for slow wave/delta events in uV
optd.maxamp=    1500;        % max amplitude for slow wave/delta events in uV
optd.detectlim= 3.0;         % detection limit in signal sd
optd.startendlim= 1;         % start/end limit for start/end times in signal sd
optd.noiselim=   35;         % exclude all signal with sd >= noiselim  
optd.clipping=  [-inf inf];  % raw uV value that indicates clipping ( = max range )
optd.ts=        [];          % limit data to ts range 
optd.lenbin= 0:0.1:3;        % bins for descriptive stats: length (=duration)
optd.ampbin= 0:25:1000;      % amplitude
optd.freqbin=0:0.2:5;        % frequency 
                 
%run
[optd,delta,dstats,dfh] = sw_run_delta_LFP(path_file);



%% spindle detection
% settings
opts=[];
opts=opt;
opts.detect=   'spindle';    % detect spindles
opts.verbose=   0;           % verbose on|off [0|1]; eg: opt.verbose=0;
opts.verbosePlus= 0;         % verbosePlus (julia's) on|off [0|1]; eg: opt.verbosePlus=0;
opts.textoutput=0;           % print text output to command line during automatic detection
opts.load=      1;           % load previously filtered/rectified raw data? [0|1]; eg opt.load=1;
opts.save=      1;           % save filtered/rectified to speed up repeated analysis? [0|1]; eg opt.load=1;
opts.reFs=      500;         % sampling rate to downsample to;  eg opt.reFs=100;
opts.filetype=  'ncs';       % data type  eg opt.filetype='ncs';
opts.filterband=[8 16];      % bandpass filter edges eg opt.filterband=[120 250];
opts.plt=       1;           % generate summary figure and print pdf [0|1]
opts.pltwin=    1;
opts.localwin=  1;           % window to find beginning and end of event with local threshold
opts.startind=  1;
opts.minlen=    0.25;        % min length of spindle in sec
opts.maxlen=    3;           % max length of spindle in sec
opts.mingap=    0.5;         % min gap to seperate spindles
opts.minamp=    25;          % min amplitude for spindle events in uV
opts.maxamp=    1000;        % max amplitude for spindle events in uV
opts.detectlim= 2.5;         % detection limit in signal sd
opts.startendlim= 0.5;       % start/end limit for start/end times in signal sd
opts.noiselim=   35;         % exclude all signal with sd >= noiselim  
opts.smoothenv= [0 0];       % apply moving_average to power envelope? yes|no nsamples [0|1 10]
opts.ts=        [];          % limit data to ts range 
opts.noise=     [];          % empty field for noise struct, optional simple mask for noisy epochs 
opts.lenbin= 0:0.1:3;        % bins for descriptive stats: length (=duration)
opts.ampbin= 0:25:500;       % amplitude
opts.freqbin=6:0.25:18;      % frequency 


% run
[opts, spindle, sstats, sfh] = sw_run_spindle_LFP(path_file,opts);



%% ripple detection
% path_file='data/csc_CA1_rest.ncs';
% 
% % settings
% optr=[];
% optr=opt;
% 
% optr.detect =    'ripple';   % detect ripples
% optroptr.verbose=   0;       % verbose on|off [0|1]; eg: opt.verbose=0;
% optroptr.verbosePlus=   0;   % verbose Plus = manual check on|off [0|1]; eg: opt.verbosePlus=0 #==off;
% optroptr.textoutput=0;       % print text output to command line during automatic detection
% optroptr.startind=   1;      % index of candidate event to start detection at 
% optr.load=      1;           % load previously filtered/rectified raw data? [0|1]; eg opt.load=1;
% optr.save=      1;           % save filtered/rectified to speed up repeated analysis? [0|1]; eg opt.load=1;
% optr.reFs=      1000;        % sampling rate to downsample to;  eg opt.reFs=500;
% optr.filetype=  'ncs';       % data type  eg opt.filetype='ncs';
% optr.filterband=[120 250];   % bandpass filter edges eg opt.filterband=[120 250];
% optr.plt=       1;           % generate summary figure and print pdf [0|1]
% optr.localwin=  0.2;         % window to find beginning and end of event with local threshold
% optr.minlen=    0.025;       % min length of ripple in sec
% optr.maxlen=    0.5;         % max length of ripple in sec
% optr.mingap=    0.1;         % min gap to seperate ripples
% optr.minamp=    50;          % min amplitude for ripple events in uV
% optr.maxamp=    1000;        % max amplitude for ripple events in uV
% optr.detectlim= 3.5;         % detection limit in signal sd
% optr.startendlim= 1;         % start/end limit for start/end times in signal sd
% optr.noiselim=   30;         % exclude signal with zscore(sig^2)>= noiselim  
% optr.smoothenv= [0 0];       % apply moving_average to power envelope? yes|no nsamples [0|1 10]
% optr.ts=        [];          % limit data to ts range 
% optr.lenbin= 0 : 0.005: 0.5; % bins for descriptive stats: length (=duration)
% optr.ampbin= 0:5:500;        % amplitude
% optr.freqbin= 100:2.5:250;   % frequency     
% 
% 
% % run 
% [optr, ripple, rstats, rfh] = sw_run_ripple_LFP(path_file,optr);



