function sc = sw_load_scoring_h (sc_fn, opt)
% funtion to import scorign for human EEG/PSG directly into sleepwalker 
opt = opt; 



%%formats: 
% luna txt files: 
% read table
sc0 = readtable (sc_fn);

sc=struct;
sc.id = sc0.ID{1};
sc.clocktime  = sc0.CLOCK_TIME;
sc.mins  = sc0.MINS;
sc.stage= sc0.STAGE;
sc.stage_no= sc0.STAGE_N;

