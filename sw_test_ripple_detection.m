% script to test sleepwalker ripple detection
close all; 
clearvars;
%% run ripple detection test

%% test loading data: add your data location here:
pathfile= 'data/csc_CA1_rest.ncs';

%load data
[dat] = sw_load_LFP (pathfile);
figure(1); clf
plot(dat.ts, dat.csc,'k');
pause(2)
close all

%% settings for ripple detection
optr=[];

%% operation mode
optr.detect='ripple';

optr.verbose= 0;             % use non interactive verbose mode, allows clicking through every event detection 
                             % verbose on|off > [0|1]; eg: opt.verbose=0;
                           
optr.verbosePlus=  0;       % use interactive verbose mode, allows *manual* scoring of detected events 
                             % verbosePlus on|off > [0|1]; eg: opt.verbosePlus=1
                             
optr.textoutput=0;           %give text output to command line during automatic detection 0|1                            
%% basic settings (need little change)
% save/load intermediate data, set both to zero to start with a clean sheet
optr.save=      0;           % save filtered/rectified to speed up repeated runs/testing parameters [0|1]; eg opt.load=1;
optr.load=      0;           % load previously filtered/rectified raw data [0|1]; eg opt.load=1; 

% data settings 
optr.ts=        [];          % limit data to timestamp range 
optr.reFs=      1000;        % sampling rate to downsample to;  eg opt.reFs=500; set to original sampling rate if not needed
optr.filetype=  'ncs';       % data type  

optr.filterband=[120 250];   % bandpass filter edges for eg opt.filterband=[120 250];
optr.plt=       1;           % generate summary figure and print pdf > [0|1]

% simple noise exclusion
optr.noiselim=   25;         % exclude signal with zscore(sig^2)>= noiselim  

%set event index to start at (for debugging)
opt.startind=1; 
%% detection settings
% initially the limits will have to be changed frequently according to your data quality
optr.detectlim=  5;         % detection limit in signal sd
optr.startendlim= 1.5;       % start/end limit for start/end times in signal sd

% the following will need to be changed less often:
optr.minlen=    0.01;        % min length of ripple in seconds
optr.maxlen=    0.5;         % max length of ripple in seconds
optr.mingap=    0.1;         % min gap to seperate ripples in seconds

optr.minamp=    50;          % min amplitude for ripple events in uV ( make sure this is consistent with your data format
                             % check sw_load_lfp.m for customising your data import and unit conversion
optr.maxamp=    1000;        % max amplitude for ripple events in uV


optr.localwin=  0.1;         % window to find beginning and end of event with local threshold
optr.smoothenv= [0 0];       % apply moving_average to power envelope? yes|no nsamples [0|1 10]

%% settings for summary histograms
optr.lenbin=0:0.005:0.5; % bins for descriptive stats
optr.ampbin=0:5:500;
optr.freqbin=100:2.5:250;         


%% run with set parameter settings, 
[optr, rip, stats, fh] = sw_run_ripple_LFP(pathfile,optr);






