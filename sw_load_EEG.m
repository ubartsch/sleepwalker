function [dat] = sw_load_EEG (opt)
% [dat] = detect_load_EEG (opt)
%--------------------------------------------------------------------------
% function to load LFP data 
% add custom load routines as needed
% 
% inputs:  1)   structure opt with fields
%             opt.pat :  path to data file in string
%                         eg opt.pat='C:\data\';
%             opt.fn  :  filename as string 
%                         eg opt.fn='csc1';
%             opt.ext :  extension of file as string
%                         eg opt.ext='ncs';
%                                  eg opt.ts=[1.36e6 2.34e6];
%
%             opt.filetype: string with file type indicator
%
%              optional fields (for Neuralynx data only):
%             opt.ts   :  pair of timestamps for limiting data load to time range 
%                          eg opt.filetype='ncs';
%
% alternatively feed in one string array with path and filename     
%            eg:  opt = D:\data\id01.edf
%                      
% outputs: 1) structure with fields
%             dat.csc  :   continuoulsy sampled channel data (signal)
%             dat.ts   :   time stamps for each csc sample
%             dat.fs   :   sampling frequency
%             (optional)
%             dat.info :   meta info on data
%
% (c) Ullrich Bartsch, University of Bristol 2014
% ubartsch(at)gmail.com

%% --------------------------------------------------------------------------
% if ~isfield(opt,'filetype') ||  isempty(opt.filetype)
%     error('   sw_detect_load_LFP  > opt has not field filetype or field is empty!')
% end

% possibiltiy to give whole file path as opt
if ischar(opt)
     dn=opt;
     opt=[];
     [dn0,fn0,ext0] = fileparts(dn);
    
    if ~isempty(ext0)
        opt.pat=[dn0 filesep];
        opt.fn=fn0;
        opt.ext=ext0;
        opt.ts=[];
    end
  
end

if ~strcmp (opt.pat(end), filesep)
    opt.pat =[ opt.pat filesep];
end

switch opt.ext 
      
        
    case '.edf'  %% human edf files 
        % set always load=0 to process original data, because of multiple
        % channels in one file
        % opt.load=0;
       
        
%         if isfield (opt, 'edf_ch')
%           
%         else
%             disp ( '    sw_load_EEG > No edf channel number set. Loading channel #1!')
%             chno=1;
%         end
        %[hd,csc]=edfread([opt.pat opt.fn opt.ext]);
        
  
            
       if isfield (opt, 'edf_ch') % load  channel selection
            % using biosig toolbox
            % check for biosig toolbox on path

            fprintf('Reading EDF format using BIOSIG...\n');
            EDF = sopen([opt.pat opt.fn opt.ext], 'r');
            [data, hdr] = sread(EDF, Inf);
            dat.csc = data';   
            % remove annotations 
            % dat.csc(end,:)=[];
            
            %strcmp chnlist 
            dat.ch_list    =  cellstr(EDF.Label);
           
             
             for i=1:length(opt.edf_ch)
                 if any( strcmp ( opt.edf_ch{i}, dat.ch_list))
                     k(i) = find (strcmp( opt.edf_ch{i}, dat.ch_list ) );
                     
                 end
             end
             dat.csc=dat.csc(k,:);
             
             dat.fs=hdr.SampleRate;
             dat.ts = linspace(0, length(dat.csc)./[dat.fs],length(dat.csc));
             dat.ch_list = dat.ch_list(k);
            
        else
                        
             fprintf('Reading EDF format using BIOSIG...\n');
            
             EDF = sopen([opt.pat opt.fn opt.ext], 'r');
             [data, hdr] = sread(EDF, Inf);
             dat.csc = data';    
             
             % get channel list only 
             dat.ch_list    =  cellstr(EDF.Label);
           
             %delete data 
             % data=[]; 
       end
        
        %dat.csc(end,:)=[];
        %dat.ch_list(end)=[];
        
        dat.fs=hdr.SampleRate;
        dat.ts = linspace(0, length(dat.csc)./[dat.fs],length(dat.csc));
        
        dat.info.fn=[  opt.fn opt.ext];
        dat.info.recordingstart=datestr(hdr.T0);
        dat.info.accessed=datestr(now);
        
       
        if isfield(opt,'ts') && ~isempty(opt.ts)
            ind= find_in_interval (opt.ts, dat.ts);
            dat.ts=dat.ts(ind);
            dat.csc=dat.csc(ind);
           
        end
        
        case '.set'
        % open EEG lab .set files
        
        % start eeglab 
        eeglab;
        
         EEG = pop_loadset( [opt.fn opt.ext], opt.pat);
        
         if isfield (opt, 'edf_ch')
             
             EEG = pop_select(EEG, 'channel', opt.edf_ch);
%
         end
        
        % id the data epoched? 
        if ~isempty(EEG.epoch)
            
            dat.csc= reshape(EEG.data, size(EEG.data,1), size(EEG.data,3)*size(EEG.data,2) );
            dat.fs=EEG.srate;
            dat.ts = linspace(0, length(dat.csc)./[dat.fs],length(dat.csc));
        
        else
            
            % convert EEG to dat
            dat.csc= EEG.data;
            dat.fs=EEG.srate;
            dat.ts = linspace(0, length(dat.csc)./[dat.fs],length(dat.csc));
        end
        
        % get ch list 
        dat.ch_list={EEG.chanlocs.labels}';
        
        
        dat.info.fn=[  opt.fn opt.ext];
%        dat.info.recordingstart=datestr(EEG.etc.T0);
        dat.info.accessed=datestr(now);
        
        %add scoring to data to tag events
        if  isfield (EEG.event,'type')
        if  any(strcmpi( unique({EEG.event.type}) ,{'N1'})) || any (strcmpi( unique({EEG.event.type}) ,{'N2'})) ...
                || any (strcmpi( unique({EEG.event.type}) ,{'N3'}))
            
            dat.scoring.stage  = {EEG.event.type};
            dat.scoring.epoch = mean(diff([EEG.event.latency]) /EEG.srate);
            lat0=EEG.event(1).latency;
            latend=EEG.event(end).latency;
            scorend= (latend- lat0)/EEG.srate ;
            dat.scoring.epochlimits = 0:dat.scoring.epoch:scorend;
                       
         
            
            
        end
        end
        
        
        
        
%%     add further custom load functions here, examples:
%             case 'abf'    
%                 https://uk.mathworks.com/matlabcentral/fileexchange/6190-abfload  
%                 [d,si]=abfload([opt.pat  opt.fn opt.ext]);
%                 dat.csc=d;
%                 dat.fs=round(1/si*1e6);
%                 dat.ts=0:si:(length(d)*si)-1;
%
     
        
   
end

%% check for correct orientation of data: we want row vectors
s=size(dat.csc);
if s(1)>s(2)
   dat.csc=dat.csc';
end    

s=size(dat.ts);
if s(1)>s(2)
   dat.ts=dat.ts';
end


disp(['    sw_load_EEG > loaded ' dat.info.fn   '!'])
  

