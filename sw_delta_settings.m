function [optd]= sw_delta_settings
% funtion to call to set delta detection settings 
% add check for calling local script?


optd.detect      = 'delta';     % detect slow wave/delta events

optd.verbose     = 0;           % verbose on|off [0|1]; eg: opt.verbose=0;
optd.again       = 0;           % do data filtering, rectifcation and zscoring again
optd.load        = 1;           % load previously filtered/rectified raw data? [0|1]; eg opt.load=1;
optd.save        = 1;           % save filtered/rectified to speed up repeated analysis? [0|1]; eg opt.load=1;
optd.reFs        = 100;         % sampling rate to downsample to;  eg opt.reFs=500;
optd.filetype    = 'ncs';       % data type
optd.filterband  = [0.5 4];     % bandpass filter edges
optd.plt         = 1;           % generate summary figure and print pdf [0|1]
optd.pltwin      = 1;           % time window to plot around events (in verbose mode)
optd.startind    = 1;           %
optd.localwin    = 0.5;         % window to find beginning and end of event with local threshold
optd.minlen      = 0.25;        % min length of slow wave/delta in sec
optd.maxlen      = 3;           % max length of slow wave/delta in sec
optd.mingap      = 0.5;         % min gap to seperate slow wave/delta
optd.minamp      = 50;          % min amplitude for slow wave/delta events in uV
optd.maxamp      = 1500;        % max amplitude for slow wave/delta events in uV
optd.detectlim   = 3.0;         % detection limit in signal sd
optd.startendlim = 1;           % start/end limit for start/end times in signal sd
optd.noiselim    = 50;       % exclude all signal with sd >= noiselim , set to 'off' if not needed
optd.clipping    = [-inf inf];  % raw uV value that indicates clipping ( = max range ) %currently not used!
optd.smoothenv   = [0 0];       % apply moving_average to power envelope? yes|no nsamples [0|1 10]
optd.ts          = [];          % limit data to ts range 

% post detection analysis options
optd.lenbin      = 0:0.1:3;      
optd.ampbin      = 0:50:2000;
optd.freqbin     = 0:0.2:5;        
