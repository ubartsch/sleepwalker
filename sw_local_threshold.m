function [istart,istop]=sw_local_threshold(env, istart0, istop0, threshwin, threshold)  
%$ function to return local threshold crossing for 

        if istart0> threshold %min n samples from start
            k1=find( env(istart0- threshold : istart0-1)<threshold  &  env(istart0- threshold+1 : istart0)>threshold, 1,'last' );
        end       
        if ~isempty(k1)
           istart(jj)=istart0-( threshold-max(k1));
        else
           if opt.verbose, disp('  > no local start threshold found'), end
           istart(jj)=istart0;
        end
             

        istop0=tmp(jj,2);  
        k2=[]; 
          
        k2=find( env( istop0 : istop0+ threshold-1 )>threshold &  env( istop0+1 : istop0+ threshold )<threshold ,1,'first');       
        if ~isempty(k2)
            istop(jj)=min(k2)-1+istop0;
        else
            if opt.verbose, disp('  > no local stop threshold found'), end
            istop(jj)=istop0;
        end