function [opt,spindle,stats] = sw_run_spindle_hEEG(varargin)
% [rip] = run_ripple_LFP(dat, opt)
% top level function to perform ripple detection 
% inputs: 
%        1) dat -  data input which can be either
%               a) a string: dat='data/human_eeg3.edf'
%                   or
%               b) a data structure with the following necessary fields:
%                   fieldnames(dat)> dat.csc, dat.ts, dat.fs
%        2) opt -  options structure with options for detection routine, see
%                 default values below.


%% 09/2019: changed from run_delta_LFP.m to accomodate human multichannel EDF

%% check inputs

close all hidden;

if nargin<1
   eval  ( [ 'help run_spindle_hEEG' ]) 
   error ( 'sleepwalker error : no arguments for run_ripple LFP! \n   Check the .m-file header for help!' )
   
elseif nargin<2
    dat=varargin{1}; % direct data input
    opt=struct;      % options generated here
elseif nargin==2
    dat=varargin{1}; % data structure
    opt=varargin{2}; % options provided
else
    eval  ( [ 'help run_spindle_hEEG' ])
    error ( ' sleepwalker error: wrong number of arguments for run_spindle_LFP! \n   Check the .m-file header for help!')
     
end




    
    
%% default values
%check options  set to standard values if not set:
opt0  =   {'signaltype',    'eeg';  % data type is human EEG
           'detect',   'spindle';     % detect spindles
           'parallel', 1;             % use parallel computing toolbox if avilable
            'verbose',   0;           % verbose on|off [0|1]; eg: opt.verbose=0;
            'verbosePlus', 0;         % verbosePlus (julia's) on|off [0|1]; eg: opt.verbosePlus=0;
            'textoutput',0;           % print text output to command line during automatic detection
            'load',      1;           % load previously filtered/rectified raw data? [0|1]; eg opt.load=1;
            'save',      1;           % save filtered/rectified to speed up repeated analysis? [0|1]; eg opt.load=1;
            'reFs',      500;         % sampling rate to downsample to;  eg opt.reFs=100;
            'filetype',  'ncs';       % data type  eg opt.filetype='ncs';
            'widepass', 0;            % do initial widepass filtering?
            'filterband',[8 16];      % bandpass filter edges eg opt.filterband=[120 250];
            'clipping', [];           % give voltage values that indicate clipping 
            'plt',       1;           % generate summary figure and print pdf [0|1]
            'pltwin',     1;
            'localwin',  1;           % window to find beginning and end of event with local threshold
            'startind',  1;
            'minlen',    0.25;        % min length of spindle in sec
            'maxlen',    3;           % max length of spindle in sec
            'mingap',    0.25;           % min gap to seperate spindles
            'minamp',    10;          % min amplitude for spindle events in uV
            'maxamp',    250;        % max amplitude for spindle events in uV
            'detectlim', 3.5;         % detection limit in signal sd
            'startendlim', 1.5;       % start/end limit for start/end times in signal sd
            'noiselim',   35;         % exclude all signal with sd >= noiselim  
            'smoothenv', [0 0];       % apply moving_average to power envelope? yes|no nsamples [0|1 10]
            'ts',        [];          % limit data to ts range 
            'noise',     [];          % empty field for noise struct, optional simple mask for noisy epochs 
            'lenbin', 0:0.1:3;        % bins for descriptive stats
            'ampbin', 0:5:250;
            'freqbin',6:0.25:18;};  
        
for i=1:length(opt0)
    if ~(isfield(opt ,opt0{i,1}))
        opt.( opt0{i,1} ) = opt0{i,2};
        disp([ '    sw > field opt.' opt0{i,1} ' not set! Using standard value: ' num2str(opt0{i,2})])
    end
end



%%
% to add
% some other settings:
% pltwin=0.2;    % size of the plotting window in verbose mode
% locthrwin=0.1; %  local threshold window


%% load data
% here we load all edf channels and process them in sequence, unless edf_ch is set:

if  ischar(dat)
    % string input : load from file
    [opt.pat, opt.fn, opt.ext] = fileparts(dat);
    opt.pat=[opt.pat '\'];
  
    [dat] = sw_load_EEG(opt) ;    
else 
    % direct data input: check struct with fields csc, ts, fs
    for i=1:length(datfields)
        if ~isfield(dat,datfields{i})
            error( [ '    sw >  dat has missing field ' datfields{i} ])
        else
            if isempty(dat.(datfields{i}))
                error( ['  > dat.' datfields{i} ' is empty!' ])
            end
        end
    end
    
    if ~isfield(opt,'pat')
          
        opt.pat = [pwd '\'];
        opt.fn  = '';
        opt.ext = '';
        disp ('    sw >  using working directory to save results!' )
    end
    
end



%% save results
if isfield(opt, 'resultsdn')  && isfield(opt, 'id')
    if ~exist(opt.resultsdn)
       mkdir(opt.resultsdn)
    end
    svdn=opt.resultsdn;
else
    svdn=opt.pat;
end

%%
if opt.widepass
    %% wide bandpass filtering : 0.5- 30 Hz
    dat= sw_filter_lfp (dat,opt);
end


%% downsample data
[dat.csc,dat.ts,dat.fs]=sw_dsample_lfp(dat,opt.reFs);

%datall=dat;


if opt.parallel==0

    for i=1:length(dat.ch_list)
        datall=dat;

        %Build single dat struct to run detection:
        dat0=dat;
        dat0.csc=dat.csc(i,:); 
        dat0.ch_list=dat.ch_list(i,:);



        %% simple noise rejection
%         manualnoisecheck=0;
%         noisewin=1;
%           dat=varargin{1}; % data structure
%         nthreshold=varargin{2}; % threshold provided
%         manreview=varargin{3}; 
%         nswin=varargin{4}*dat.fs;
%         satval = varargin{5}; 
%         exclude= varargin{6}; 
        % [opt.noise]=sw_simple_noise_rejection(dat,opt.noiselim, 0);
        % [opt.noise,dat0]=sw_simple_noise_rejection(dat0,opt.noiselim, opt.manualnoisecheck, opt.noisewin, opt.clipping,opt.exclude);
       
        [opt.noise,dat0]=sw_noise_rejection(dat0, opt);
        %% filter & envelope
        [dat0,opt]= sw_get_env(dat0,opt);


        %% run detections
        [spindle0{i}]=sw_detect_spindle(dat0,opt);

        %% basic analysis 
        [stats0{i}]=sw_get_stats(dat0,spindle0{i},opt);

        if opt.plt
        %% mk summary figure
            [fh, fnchpdf{i}]= sw_plot_detection(dat0,spindle0{i},stats0{i},opt);
        else
            fh=[];
        end

    %     if i==1
    %          print ([opt.pat opt.fn '_' opt.detect '.ps'], '-dpsc2')    
    %     else
    %          print ([opt.pat opt.fn '_' opt.detect '.ps'], '-dpsc2','-append')
    %     end 
    %      
        %save results
    %     svfn=[opt.pat opt.fn '_' dat0.ch_list{1} '_' opt.detect  '_results.mat'];
    %     save(svfn , 'spindle','opt','stats')
    %     fprintf ('    sw >  saved  %s \n\n' , svfn )

    end


elseif opt.parallel==1

    
    pj = gcp('nocreate');
    if isempty(pj)
        if isfield(opt, 'numparworkers')
             pj = parpool('local', opt.numparworkers);
        else
             nc=feature('numcores');
             pj = parpool('local', nc/2);    
        end
    end
    spindle0=cell(length(dat.ch_list),1);
    
    parfor i=1:length(dat.ch_list)


        %Build single dat struct to run (parallel) detection:
        dat0=dat;
        dat0.csc=dat.csc(i,:); 
        dat0.ch_list=dat.ch_list(i,:);

        opt0=opt;
        delta0=cell(1);

        %% simple noise rejection
        %manualnoisecheck=0;
        % [opt.noise]=sw_simple_noise_rejection(dat,opt.noiselim, 0);
%         dat=varargin{1}; % data structure
%         nthreshold=varargin{2}; % threshold provided
%         manreview=varargin{3}; 
%         nswin=varargin{4}*dat.fs;
%         satval = varargin{5}; 
%         exclude= varargin{6}; 
        %[opt0.noise,dat0]=sw_simple_noise_rejection(dat0, opt0.noiselim, opt0.manualnoisecheck, opt.nswin, opt.satval, opt.ecxclude);
        [opt0.noise,dat0]=sw_noise_rejection(dat0, opt0);


        %% filter & envelope
        [dat0,opt0]= sw_get_env(dat0,opt0);


        %% run detections
        try 
            [spindle0{i}]=sw_detect_spindle(dat0,opt0);
        catch me 
            spindle0{i}=me;
            %disp (dat.ch_list)
            %disp (me)
        end
        
        
        
        if isstruct (spindle0{i})
            %% basic analysis 
            [stats0{i}]=sw_get_stats(dat0,spindle0{i},opt0);

            if opt0.plt
            %% mk summary figure
                [fh, fnchpdf{i}]= sw_plot_detection(dat0,spindle0{i},stats0{i},opt0);
            else
                fh=[];
            end
         else
            stats0{i}=struct;
            %fnchpdf{i}='';
            
        end
        



    end
    
    

end 



%% collect results into structure for easier readability

for i=1:length(spindle0)
    spindle.(matlab.lang.makeValidName ( dat.ch_list{i}) )=spindle0{i} ;
    stats.(matlab.lang.makeValidName ( dat.ch_list{i}) ) =stats0{i};
end




if  isfield(opt, 'id')      
    svfn=[svdn opt.id '_'  opt.detect  '_results.mat'];
else
    svfn=[svdn opt.fn '_'  opt.detect  '_results.mat'];
end

save(svfn , opt.detect ,'opt','stats')
fprintf ('    sw >  saved  %s \n\n' , svfn )

% create final pdf with all results!
if length(fnchpdf)>1
    fnpdf=[svdn opt.fn '_' opt.detect  '_all_results.pdf'];
    for i=1:length(fnchpdf)
        fnchpdf{i}=[fnchpdf{i} '.pdf'];
    end
    merge_pdfs (sort(fnchpdf),fnpdf);
    
    
end
for i=1:length(fnchpdf); delete(fnchpdf{i}) ; end






