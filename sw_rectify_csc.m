function [dat]=sw_rectify_csc(dat,av,noise,sd)
% [dat]=sw_rectify_csc(dat,band,av,noise,sd)
% rectify single csc
% uses eeglab eegfilt function!
% inputs: dat = single csc channel as vector
%          cFs = samplig rate in Hz, eg >> cFs=1024;
%          band = filter band values in Hz, eg >> band=[8 15];  
%          av = flag to calc moving average with x sec windows
%          eg >> av=[1 0.5]; mov_av with .5 sec win
% outputs: csx.f = filtered trace
%          csx.fz = z-score of filtered trace
%          csx.r = rectified trace
%          csx.rz = z-score of rectified trace
% (c) Ullrich Bartsch, University of Bristol 2014
% ubartsch(at)gmail.com
%% --------------------------------------------------------------------------

if size(dat.csc,1)>size(dat.csc,2) 
    dat.csc=dat.csc';
end

if size(dat.f,1)>size(dat.f,2) 
    dat.f=dat.f';
end
% rectify for event detection:     

if isempty(sd)
  dat.r= sqrt((dat.f).^2 ); 
  [dat.rz,dat.rzmean, dat.rzstd]=sw_zscore(dat.r,2,noise);

else
   % if signal is part of a longer acquisition, one migth want to normalise
   % by a baseline SD  or similar
   [dat.rz, dat.rzmean, dat.rzstd] =zscore_externalSD(dat.r,2,noise,sd);
end


if av(1)
    dat.rz=moving_average(dat.rz,av(2)*cFs/2);
end

