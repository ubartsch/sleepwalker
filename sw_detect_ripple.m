function [rip]=sw_detect_ripple(dat,opt)
%% ------------------------------------------------------------------------
%  [rip,cscx,csc,cts]=sw_detect_ripple(dat,opt)
%
% Function to detect ripple events in a lfp trace. 
%
% The input signal is an envelope of power (raw signal is zscored -mean removed,
% divided by sd-, rectified, spline fitted envelope generated)
% and canditate events are detected by using a hard threshold 
% (the detection limit).
%
% Candidate events are then tested if they comply with criteria set in the
% options structure and then added to a final set of events.
% The results are saved as a matlab file [fn0 '_ripple_res.mat'] in the
% source directory of the data file for further analysis.
% 
% There are two modes available:
%  - opt.verbose=1; verbose mode allows the step by step 
%                   inspection of every single detection 
%                   with detailed command line output on detected events
%                   [to be added: and manual acceptance for each event]
%
%  - opt.verbose=0; automatic mode, runs the whole detection without user
%                   interaction, only presents a summary figure at the end
%                   (if selected)
%
% inputs: 
%   - dat: either string argument with filename incl path
%          eg : dat='E:\data\xyz.ncs'
%          structure with fields
%             dat.env  :   envelope of bandpassed signal of interest 
%             dat.csc  :   continuoulsy sampled channel data (raw signal)
%             dat.ts   :   time stamps for each csc sample
%             dat.fs   :   sampling frequency
%             (optional)
%             dat.info :   meta info on data  
%
%   - opt: structure array with custom options:
%             see sw_run_ripple.m for default values 
% 
%
% 0.21, Oct 2017,  added manual scoring of events, thanks to Julia Heckenast
% 0.2, Apr 2015 beta release
%      - added extended amplitude criterium: signal has to be above
%        threshold for at least 25% of the event time
%     
% 0.1, Nov 2010
% Ullrich Bartsch, University of Bristol
%% --------------------------------------------------------------------------

% changelog
% fixed non-increasing index when combining two events into one, 12/10/16
% added startind option,12/10/16
% fixed noise exclusion of events, 12/10/16 
%% --------------------------------------------------------------------------
% check if envelope has been calculated and added as field to dat
if ~isfield (dat, 'env')
   eval  ( [ 'help sw_detect_ripple' ]) 
   error ( '\n   sleepwalker error  \n   No envelope found!' )
end

% ripple detection
pltwin=0.1;
sethrwin=floor(opt.localwin*dat.fs); 
perc_above_thr=25;

ylims=[-5 40];
yraw=40;
yfilt=30;

rip=[];

%% create a noise mask from very high sd epochs in raw data

% detect threshold crossings:
start_ind = find(dat.env(1:end-1)<opt.detectlim & dat.env(2:end)>opt.detectlim );
stop_ind = find(dat.env(1:end-1)>opt.detectlim & dat.env(2:end)<opt.detectlim  );

% ignore starts without matching stops 
while stop_ind(1)< start_ind(1);
    stop_ind(1)=[];
end;
while start_ind(end)>stop_ind(end);
    start_ind(end)=[];
end;

% ignore events within noise epochs:
if ~isempty(opt.noise)
    %isnoise=find((dat.env>opt.noiselim)); 
    noiseind=[opt.noise.ind];
    
    hasnoise1=ismember(start_ind,noiseind);
    hasnoise2=ismember(stop_ind,noiseind);
    
    start_ind(hasnoise1&hasnoise2)=[]; stop_ind(hasnoise1&hasnoise2)=[];
    disp('    sleepwalker')   
    disp(['   > removed ' num2str(length(find(hasnoise1&hasnoise2))) ' events within noisy epeochs!'])  
    disp(' ')
end

if (opt.verbose || opt.verbosePlus)
%    close (mh)
    figure(1);
   % set(0,'CurrentFigure',1)
    clf;
    set(gcf,'color','w','position',[ 15   473   718   485])
    plot(dat.ts,dat.rz,'k')
    hold on
    plot(dat.ts([opt.noise.ind]),dat.z([opt.noise.ind])+yraw,'.', 'color',[.8 .8 .8]) % edited  by Julia
    plot(dat.ts,dat.z+yraw,'color',[.3 .3 .3],'linewidth', 2);
    plot(dat.ts,dat.fz+yfilt,'color',[.2 .2 .2],'linewidth', 2);
    plot(dat.ts,dat.env,'k');
    plot(dat.ts,ones(size(dat.ts))*(opt.detectlim),'r','linewidth',2)
    plot(dat.ts,ones(size(dat.ts))*(opt.startendlim),'r--','linewidth',2)
    plot(dat.ts(start_ind),dat.env(start_ind),'go');
    plot(dat.ts(stop_ind),dat.env(stop_ind),'bo');
    set(gca,'ylim',[0 50]);
    hold off
    formatfig
    
    if opt.verbose
      
        
        disp('##########################################################################')
        disp('                   Ripple detection: Verbose mode!'  )
        disp('##########################################################################')
        disp(' ')
        disp('                    Hit any key to step through!')
        disp(' ')
    end
end


%threshold crossing count  
if opt.startind~=1
    i=opt.startind;
else
    i=1;
end

j=1;%final event count
 

if isfield(opt,'startind')
    i=opt.startind;
end

if ~opt.verbose || opt.verbosePlus
    %mh=waitbar(0);
end
max_n=length(start_ind);
tmp=zeros(size(length(start_ind),2));

%%  lets check them out
while i<length(start_ind)-1 
    
    %% calc some tmp features
  
    
    rip_len=dat.ts(stop_ind(i))-dat.ts(start_ind(i));
    rip_gap=dat.ts(start_ind(i+1))-dat.ts(stop_ind(i));

    if (opt.verbose || opt.verbosePlus) % disp some info on timing 
        fprintf ('  >  threshold crossing #%g:   ts %g, length %g, gap %g\n',i, dat.ts(start_ind(i)), rip_len, rip_gap);
        %disp (['crossing ' num2str([ i  start_ind(i) stop_ind(i) rip_len rip_gap    ] ) ]);
        hold on
        set(0,'CurrentFigure',1)
        plot(dat.ts(start_ind(i):stop_ind(i)),dat.rz(start_ind(i):stop_ind(i)),'r');
        hold off
        axis ([dat.ts(start_ind(i))-pltwin,dat.ts(stop_ind(i))+pltwin,-2 50])
        %pause
    end
    
    %% start selecting events based on their duration (length in time)
    ii=0;% future event check count
    detect=0; %detection flag
    tmp_start=[]; tmp_stop=[];
    
    %  event is within length criteria
    if  (rip_len>=opt.minlen) && (rip_len<=opt.maxlen)
        
        if opt.verbose ||opt.verbosePlus
            disp('   >>  rip_len > min length! ' );
        end
        
        tmp_start=start_ind(i);
        tmp_stop=stop_ind(i);
        %rip_gap=dat.ts(start_ind(i+1))-dat.ts(stop_ind(i));
        
        % but are there other events close by?  combine into one if yes       
        while (i+ii+1 <= max_n) && (dat.ts(start_ind(i+ii+1))-dat.ts(stop_ind(i+ii)) <= opt.mingap)             
            gap0=dat.ts(start_ind(i+ii+1)) - dat.ts(stop_ind(i+ii));
            
            if (opt.verbose || opt.verbosePlus) && ii>0
                %disp( [ '   ' ] )
                fprintf ('   > within duration but next crossing #%g < mingap:  gap %g\n',i+ii+1, gap0);
            end
            
            tmp_stop=stop_ind(i+ii+1); % replace stop_ind with current ind
            ii=ii+1;                   % check the next one
        end
        
        
        detect=1;
        
   
    % event is too short but next event is close by:
    elseif (rip_len<opt.minlen) && (rip_gap <= opt.mingap) && (i+1 <= max_n)    
         
        tmp_start=start_ind(i);
        tmp_stop=stop_ind(i);
        
        if opt.verbose ||opt.verbosePlus 
            fprintf ('  >  event #%g is below min length, but next crossing #%g < mingap:  gap %g\n',i, i+ii+1, rip_gap);
        end
       
           
        % are there other events close by?  combine into one if yes   
        while  (dat.ts(start_ind(i+ii+1)) - dat.ts(stop_ind(i+ii)) < opt.mingap) && (i+ii+1 < max_n-2)  % more than one event close by?
            gapn=dat.ts(start_ind(i+ii+1))-dat.ts(stop_ind(i+ii));
            lenn= dat.ts(stop_ind(i+ii+1))-dat.ts(start_ind(i+ii+1));
            if (opt.verbose || opt.verbosePlus) && ii>0
                fprintf ('     >>  next crossing #%g < mingap:  gap %g,  length %g \n',i+ii+1, gapn,lenn);
            end          
            tmp_stop=stop_ind(i+ii+1);
            ii=ii+1;
        end
    
        if        ((dat.ts(tmp_stop) - dat.ts(tmp_start)) >= opt.minlen)... % is the final event long enough but not too long?
               && ((dat.ts(tmp_stop) - dat.ts(tmp_start)) <= opt.maxlen)...
               
             detect=1;
          
        else  
            
            if (opt.verbose || opt.verbosePlus) 
                fprintf ( '     >>  combined event is too short/long in total: len %g2.2 \n' ,(dat.ts(tmp_stop) - dat.ts(tmp_start)) );
            end
            detect=0;
            i=i+1;
        end
       
    else 
        
         if (opt.verbose || opt.verbosePlus) 
             fprintf ( '     >>  event is too short/long (no detection) : len %g2.2 \n' ,rip_len );
         end
         detect=0;
         i=i+1;
    end
    
    
%% check properties
    if detect
             if (opt.verbose || opt.verbosePlus)
                 axis ([dat.ts(tmp_start)-pltwin,dat.ts(tmp_stop)+pltwin,-2 50])
                 pause(0.2)
             end   
          %compute simple max min amplitude
            [mx0]=max( dat.fz(tmp_start:tmp_stop) );
            [mn0]=min( dat.fz(tmp_start:tmp_stop) );
            amp0=sqrt(( mx0 - mn0 )^2 );  
            
            nabove= length( find( dat.env(tmp_start:tmp_stop)>opt.detectlim ) );
            totlen=  length( dat.env(tmp_start:tmp_stop) );
       
            
            if  (amp0*dat.fzstd>opt.minamp) &&...
                (amp0*dat.fzstd<opt.maxamp) &&...            %(amp0<opt.noiselim) && ...                
                (100/totlen)*nabove >= perc_above_thr  && ...    % check if signal is at least 50% of the time above threshold
                totlen/dat.fs <  opt.maxlen && ...
                totlen/dat.fs >  opt.minlen    && ...
                tmp_stop < length(dat.csc)- dat.fs
                                                          
                  
                % move indices forward  if properties are ok
                i=i+ii+1;
                j=j+1;
                
                detect=1;
           
            else
                
                detect=0;
                %tmp(j,:)=[];
                %if ii>0
                i=i+ii+1;
                %else
                %    i=i+1;
                %end
                
                if (opt.verbose || opt.verbosePlus)
                    
                       
%                    text(dat.ts(start_ind(i-1)), 49, ['#' num2str(i-1)] );
%                    text(dat.ts(start_ind(i-ii-1)), 49, ['#' num2str(i-ii-1)] );
%                    text(dat.ts(stop_ind(i)), 47, ['#' num2str(jj)],'color',[.2 .6 .2] ); 
                   
                   disp ('   > some final criteria not met! ')
                   disp( ['     amp : ' num2str(amp0*dat.fzstd)   '  perc above : ' num2str(nabove/totlen) '     len: ' num2str(totlen/dat.fs)  ]);
                   
                end
                
               
                      

            end  
            
%             if (opt.verbose || opt.verbosePlus)
%                disp (['   > z amp='  num2str(amp0)  '  abs amp=' num2str(amp0*dat.fzstd) ])
%                disp (['   > total length='  num2str( totlen/dat.fs) '   percentage above threshold = ' num2str((nabove/totlen)*100) ])
%             end
    end   
 

        
    %% we like what we got, make it so 
    if detect
        
            
        %% use local threshold to get final start and end times
        %  
        jj=j-1;% look at the current detection    
        k1=[]; 
        istart0=tmp_start;
        if istart0>sethrwin %min n samples from start
            k1=find( dat.env(istart0-sethrwin : istart0-1)<opt.startendlim  &  dat.env(istart0-sethrwin+1 : istart0)>opt.startendlim, 1,'last' );
        end       
        if ~isempty(k1)
           istart(jj)=istart0-(sethrwin-max(k1));
        else
           if opt.verbose, disp('  > no local start threshold found'), end
           istart(jj)=istart0;
        end
             
        istop0=tmp_stop;  
        k2=[]; 
          
        k2=find( dat.env( istop0 : istop0+sethrwin-1 )>opt.startendlim &  dat.env( istop0+1 : istop0+sethrwin )<opt.startendlim ,1,'first');       
        if ~isempty(k2)
            istop(jj)=min(k2)-1+istop0;
        else
            if opt.verbose, disp('  > no local stop threshold found'), end
            istop(jj)=istop0;
        end
   
        
        % all local maxima & minima
        [mx0,mxk0]=findpeaks(  dat.f(istart(jj):istop(jj)) );
        [mn0,mnk0]=findpeaks( -dat.f(istart(jj):istop(jj)) );
      
        indallmax=istart(jj)+mxk0-1;
        indallmin=istart(jj)+mnk0-1;
          
        % absoulute local maximum & minimum
        [mmx,mmxk]=max(mx0);
        [mmn,mmnk]=max(mn0);
           
        indabsmax=istart(jj)+mxk0(mmxk)-1;
        indabsmin=istart(jj)+mnk0(mmnk)-1;
        
        %% collect results
        rip(jj).indall=istart(jj):istop(jj);
        rip(jj).indabsmax=indabsmax;
        rip(jj).indabsmin=indabsmin;
     
        
        rip(jj).tsstart=dat.ts(istart(jj));
        rip(jj).tsstop=dat.ts(istop(jj));
        
        rip(jj).tsabsmax=dat.ts(indabsmax);
        rip(jj).tsabsmin=dat.ts(indabsmin);
        
        rip(jj).tsallmax=dat.ts(indallmax);
        rip(jj).tsallmin=dat.ts(indallmin);
        
       % amp1=abs(dat.f(indabsmax)) + abs(dat.f(indabsmin));
        
        rip(jj).amplitude=amp0*dat.fzstd; %
        rip(jj).length=dat.ts(istop(jj))-dat.ts(istart(jj));       
        rip(jj).freq=1/mean(diff(dat.ts(indallmax)));
        
        %rip(jj).instamp=abs (dat.hil(rip(jj).indstart : rip(jj).indstop));
        %rip(jj).instfreq=(dat.instf(rip(jj).indstart : rip(jj).indstop));
        
        if opt.textoutput
            fprintf ('   > %g ripple:   ts %g, amp=%g, f=%g, len=%g \n',...
                jj, rip(jj).tsstart,  rip(jj).amplitude ,  rip(jj).freq , rip(jj).length );
        end
        
        if (opt.verbose || opt.verbosePlus)
               disp(' ');
               disp (['   > checking candidate event properties #'  num2str(i-ii-1)])
               disp (['   > z amp='  num2str(amp0)  '  abs amp=' num2str(amp0*dat.fzstd) ])
               disp (['   > total length='  num2str( totlen/dat.fs) '   percentage above threshold = ' num2str((nabove/totlen)*100) ])
            
            %disp([amp0*dat.fzstd amp1])
            
            %figure(1);
            set(0,'CurrentFigure',1)
            hold on      
            
            text(dat.ts(start_ind(i-1)), 49, ['#' num2str(i-1)] );
            text(dat.ts(start_ind(i-ii-1)), 49, ['#' num2str(i-ii-1)] );
            text(dat.ts(istop(jj)), 47, ['#' num2str(jj)],'color',[.2 .6 .2] );
            
            axis ([dat.ts(istart(jj))-pltwin,dat.ts(istop(jj))+pltwin,-2 50])
     
            plot(dat.ts(istart(jj):istop(jj)) , dat.rz(istart(jj):istop(jj)) ,'b');
            plot(dat.ts(istart(jj):istop(jj)) , dat.z(istart(jj):istop(jj))+yraw,'b');
            plot(dat.ts(istart(jj):istop(jj)) , dat.fz(istart(jj):istop(jj))+yfilt,'b');
            
            plot(dat.ts(istart(jj)),dat.env(istart(jj)),'g+');
            plot(dat.ts(istop(jj)),dat.env(istop(jj)),'b+');

            plot(dat.ts(indallmax),dat.fz(indallmax)+yfilt,'yO');
            plot(dat.ts(indallmin),dat.fz(indallmin)+yfilt,'mO');

            plot(dat.ts(indabsmax),dat.fz(indabsmax)+yfilt,'r>');
            plot(dat.ts(indabsmin),dat.fz(indabsmin)+yfilt,'b<');
            
            hold off
            drawnow

            if opt.verbose
                disp('---')
                fprintf ('   > final ripple event #%g: amp %g, len %g, freq %g \n',jj,  rip(jj).amplitude , rip(jj).length, rip(jj).freq);
                %disp(['final ' num2str([ jj tmp2(jj,:)]) ] ) 
                disp('---') 
                pause
            end

        end
        
        
       % final manual check on detected events only 
        if opt.verbosePlus %verbosePlus = 1
            str='0';
            
            while strcmp(str, '0')
                disp('   > Manual event check:') 
                prompt = '   > Select event type: seizure = s. ripple = r. noise = n. other = o. ';
                str = input(prompt,'s');
               
                %If user presses enter without entering a character,
                %str will be empty, causing the while loop to fail. So if
                %str is empty, put a dummy character to make the code
                %through the loop again
                if(isempty(str) || length(str)>=2)
                    str='0';
                end

                switch str
                    case 's'
                        man(jj).seizure = 1;
                        man(jj).ripple = 0;
                        man(jj).noise= 0;
                        man(jj).other = 0;                      
                        disp('    >> seizure')
                    case 'r'
                        man(jj).seizure = 0;
                        man(jj).ripple = 1;
                        man(jj).noise= 0;
                        man(jj).other = 0;
                        disp('    >> ripple')
                    case 'n'
                        man(jj).seizure = 0;
                        man(jj).ripple = 0;
                        man(jj).noise= 1;
                        man(jj).other = 0;
                        disp('    >> noise')
                    case 'o'
                        man(jj).seizure = 0;
                        man(jj).ripple = 0;
                        man(jj).noise= 0;
                        man(jj).other=1;
                        disp('    >> other')
                    otherwise
                        fprintf('   > Invalid answer! Score again...\n');
                        str='0';
                        
                     
                end
               pause(0.5)
               %pause
            end
            %man.start=man.start+1;
            man(jj).tsstart=dat.ts(istart(jj));
            man(jj).tsstop=dat.ts(istop(jj));
            man(jj).tsabsmax=dat.ts(indabsmax);
            man(jj).tsabsmin=dat.ts(indabsmin);
            man(jj).indabsmax=indabsmax;
            man(jj).indabsmin=indabsmin;
            man(jj).tsallmax=dat.ts(indallmax);
            man(jj).tsallmin=dat.ts(indallmin);
            man(jj).amplitude=abs (dat.f(indabsmax))+ abs( dat.f(indabsmin));
            man(jj).length=dat.ts(istop(jj))-dat.ts(istart(jj));
            man(jj).freq=1/mean(diff(dat.ts(indallmax)));
            
            disp(' ');

            %save('manions.mat','man');
            %jj=jj+1;
        end
        
    %%check if event overlpas with previous event    
%     if    rip(jj).tsstart <= rip(jj-1).tsstop 
%         rip(jj-1)=rip(jj);
%         rip(jj)=[];
%     end
    
    
    end
    if ~(opt.verbose || opt.verbosePlus)
%         waitbar(i/max_n,mh,['Checked ' num2str(i) ' of ' num2str(max_n) ' threshold crossings!  Press Ctrl-C to interrupt.'])
        progressbar(i/(max_n+20),1)
    end
    if (opt.verbose || opt.verbosePlus)
        disp(' ')
    end
end

if ~(opt.verbose || opt.verbosePlus)
    
    if ~exist('jj', 'var')
        jj=0;
        rip=[];
        disp(['No ripples detected ! Check settings and or data!'])
      
        
    else
        disp(['Detected ' num2str(jj) ' ripples out of ' num2str(max_n) ' threshold crossings!  Thank you and good night!'])
      
    end
% waitbar(i/max_n,mh)

progressbar(1,1)
pause(2)
%close (mh)
end     

if opt.verbosePlus
    rip0.man=man;
    rip0.auto=rip;
    rip=rip0;
end


warning on all
