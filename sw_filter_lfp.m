function dat= sw_filter_lfp (dat, opt)
% function to filter lfp before downsampling 

% check for previously filtered data
if isfield (dat,'info')  % widepass
    
    if isfield (opt,'pat') && isfield (opt,'detect') && isfield (dat,'info') &&  ... 
       exist([ opt.pat  dat.info.fn(1:end-4) '_' opt.detect  '_widepass.mat'],'file')==2 && opt.load==1
        
            
        load([ opt.pat dat.info.fn(1:end-4) '_' opt.detect  '_widepass.mat']);
        disp (  '    sw >  loaded previously saved filtered dataset! Set opt.load=0 to disable!')

    else
        
        
        
        disp ([ '    sw > wide bandpass filtering...: '])
       
        %mock eeglab struct 
        EEG.data=dat.csc;
        EEG.srate=dat.fs;
        EEG.trials=[];
        EEG.pnts=size(dat.csc,2);
        EEG.event=[];
        EEG.nbchan=size(dat.csc,1);
        % switch to pop_eegfiltnew
        [EEG] = pop_eegfiltnew(EEG, 0.25, []) ;
        
        %dat.csc= eegfilt(dat.csc,dat.fs,0.25);
        %temp add to eeg struct
  
        switch  opt.detect
            case 'delta'  
                  %dat.csc= eegfilt(dat.csc,dat.fs,0,10);
                  [EEG] = pop_eegfiltnew(EEG, [], 10) ;
                 
            case 'spindle'        
                   %dat.csc= eegfilt(dat.csc,dat.fs,0,35);
                    [EEG] = pop_eegfiltnew(EEG, [], 35) ;
            case 'ripple'
                
                   %dat.csc= eegfilt(dat.csc,dat.fs,0,400);
                    [EEG] = pop_eegfiltnew(EEG, [], 400) ;
        end
        dat.csc= EEG.data;
        clear EEG    
    
        if isfield (opt,'save') && opt.save  % save filtered data to mat file for faster re-loading
            % 
            disp ([ '   sw > saving wide bandpass filtered data to: ' dat.info.fn(1:end-4) '_' opt.detect  '_widepass.mat' ])
            save([ opt.pat dat.info.fn(1:end-4) '_' opt.detect  '_widepass.mat'],'dat','-v7.3')
        end
    end


% else % bandpass
%      dat.csc= eegfilt(dat.csc,dat.fs,0.5,0);
% 
%     switch  opt.detect
%         case 'delta'       
%                dat.csc= eegfilt(dat.csc,dat.fs,0,10);
%         case 'spindle'        
%                dat.csc= eegfilt(dat.csc,dat.fs,0,35);          
%         case 'ripple'
%                dat.csc= eegfilt(dat.csc,dat.fs,0,400);
%     end
end