function [opt, spindle, stats, fh] = sw_run_spindle_LFP(varargin)
% [spindle] = sw_run_spindle_LFP(dat, opt)
% top level function to perform ripple detection 
% inputs: 
%        dat  - data input which can be either
%               a) string with path/file.dat or
%               b) a data structure with the following fields:
%                  dat.csc, dat.
%        opt  - options structure with setttings for ripple analysis
%               see default values below
% outputs:
%       opt   - option structure with options used to generate the current
%               results 
%       spin  - results structure with one field for each spindle event
%       stats - results structure with descriptive stats from all spindles
%               detected.
%       fh    - handle for figure (if opt.plt is set.)

% example: [spin] = sw_run_spindle_LFP(dat, opt)
% (c) Ullrich Bartsch, University of Bristol 2015
% ubartsch(at)gmail.com
%% --------------------------------------------------------------------------
%sw_disp_info


if nargin<1
   eval  ( [ 'help run_spindle_LFP' ]) 
   error ( 'sleepwalker error : no arguments for run_ripple LFP! \n   Check the .m-file header for help!' )
   
elseif nargin<2
    dat=varargin{1}; % direct data input
    opt=struct;      % options generated here
elseif nargin==2
    dat=varargin{1}; % data structure
    opt=varargin{2}; % options provided
else
    eval  ( [ 'help run_spindle_LFP' ])
    error ( ' sleepwalker error: wrong number of arguments for run_spindle_LFP! \n   Check the .m-file header for help!')
     
end
            

%% some other settings:
pltwin=0.25;    % size of the plotting window in verbose mode
locthrwin=0.2; %  local threshold window


% set to default values if not set   
% default values for options
% check options  set to standard values if not set:
opt0  =     {'detect',   'spindle';   % detect spindles
            'verbose',   0;           % verbose on|off [0|1]; eg: opt.verbose=0;
            'verbosePlus', 0;          % verbosePlus (julia's) on|off [0|1]; eg: opt.verbosePlus=0;
            'textoutput',0;           % print text output to command line during automatic detection
            'load',      1;           % load previously filtered/rectified raw data? [0|1]; eg opt.load=1;
            'save',      1;           % save filtered/rectified to speed up repeated analysis? [0|1]; eg opt.load=1;
            'reFs',      500;         % sampling rate to downsample to;  eg opt.reFs=100;
            'filetype',  'ncs';       % data type  eg opt.filetype='ncs';
            'filterband',[8 16];      % bandpass filter edges eg opt.filterband=[120 250];
            'plt',       1;           % generate summary figure and print pdf [0|1]
            'pltwin',     1;
            'localwin',  1;           % window to find beginning and end of event with local threshold
            'startind',  1;
            'minlen',    0.25;        % min length of spindle in sec
            'maxlen',    3;           % max length of spindle in sec
            'mingap',    0.25;           % min gap to seperate spindles
            'minamp',    25;          % min amplitude for spindle events in uV
            'maxamp',    1000;        % max amplitude for spindle events in uV
            'detectlim', 2;           % detection limit in signal sd
            'startendlim', 0.5;       % start/end limit for start/end times in signal sd
            'noiselim',   35;         % exclude all signal with sd >= noiselim  
            'smoothenv', [0 0];       % apply moving_average to power envelope? yes|no nsamples [0|1 10]
            'ts',        [];          % limit data to ts range 
            'noise',     [];          % empty field for noise struct, optional simple mask for noisy epochs 
            'lenbin', 0:0.1:3;        % bins for descriptive stats
            'ampbin', 0:25:500;
            'freqbin',6:0.25:18;
            'satval', [];
            'exclude', [];
            };        


for i=1:length(opt0)
    if ~(isfield(opt ,opt0{i,1}))
        opt.( opt0{i,1} ) = opt0{i,2};
        disp([ '   >  opt.' opt0{i,1} ' not set! Using standard value: ' num2str(opt0{i,2})])
    end
end

%% load data
datfields={'csc','ts','fs','dn','fn'};
if  ischar(dat)
    % string input : load from file
    [opt.pat, opt.fn, opt.ext] = fileparts(dat);
    opt.pat=[opt.pat '\'];
    [dat] = sw_load_LFP(opt) ;
    
elseif isstruct(dat)
    
    % direct data input: check struct with fields csc, ts, fs
    
    dat.dn= [pwd '\'];
    dat.fn='csc';
    
    for i=1:length(datfields)
        if ~isfield(dat,(datfields{i}))
            error( [ '  > dat is missing field ' datfields{i} ])
        else
            if isempty(dat.(datfields{i}))
                error( ['  > dat.' datfields{i} ' is empty!' ])
            end
        end
    end
    
    if ~isfield(opt,'pat')
          
        opt.pat = [pwd '\'];
        opt.fn  = '';
        opt.ext = '';
        disp ('  > using working directory to save results!' )
    end
    
    if ~isempty(opt.ts)
       k=dat.ts>opt.ts(1) & dat.ts<opt.ts(2) ;
       dat.ts=dat.ts(k);
       dat.csc=dat.csc(k);
    end   

end




%% start with wideband filter on raw data
dat= sw_filter_lfp (dat, opt);

%% downsample data
% saving filtered data to fn_filt_event.mat
[dat.csc,dat.ts,dat.fs]=sw_dsample_lfp(dat,opt.reFs);

%% simple noise rejection
manualnoisecheck=0;
% [opt.noise]=sw_simple_noise_rejection(dat,opt.noiselim, 0);
[opt.noise,dat]=sw_simple_noise_rejection(dat,opt.noiselim, manualnoisecheck, opt.noisewin, opt.satval, opt.exclude);

%% filter, rectify and calculate envelope
% insert custom function here to calc signal envelope
[dat,opt]= sw_get_env(dat,opt);% added as dat.env to dat structure

%% run detection
[spindle]=sw_detect_spindle(dat,opt);

%% basic analysis 
[stats]=sw_get_stats(dat,spindle,opt);

if opt.plt
%% mk summary figure
    [fh]= sw_plot_detection(dat,spindle, stats,opt);
else
    fh=[];
end


%% save the final detection structure
ext = '.mat';
if isfield (opt,'suffix') && ~isempty(opt.suffix)
   suf = opt.suffix;
else
   suf ='';
end

if isfield (dat,'ch_list') && ~isempty(dat.ch_list)
    svfn=[opt.pat opt.fn '_' dat.ch_list{1} '_' opt.detect '_results' ];
else  
    svfn=[opt.pat opt.fn '_' opt.detect '_results' ];
end

 %save results
save([svfn suf ext] , 'spindle','opt','stats')
fprintf ('   > saved  %s \n\n' , [ svfn suf ext])


%save matlab figure?
%savefig(fh,[svfn(1:end-4) '.fig'])

%rem temp data?
%[out] = sw_rem_temporary_data(opt);

end

