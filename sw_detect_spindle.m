function [spin]=sw_detect_spindle(dat,opt)
%% ------------------------------------------------------------------------
%  [spin,dat,dat,dat.ts]=sw_detect_spindle(dat,opt)
%
% Function to detect spindle events in a lfp trace.
%
% The input signal is an envelope of power (raw signal is zscored -mean removed,
% divided by sd-, rectified, spline fitted envelope generated)
% and canditate events are detected by using a hard threshold
% (the detection limit).
%
% Candidate events are then tested if they comply with criteria set in the
% options structure and then added to a final set of events.
% The results are saved as a matlab file [fn0 '_spindle_res.mat'] in the
% source directory of the data file for further analysis.
%
% There are two modes available:
%  - opt.verbose=1; verbose mode allows the step by step
%                   inspection of every single detection
%                   with detailed command line output on detected events
%                   [to be added: and manual acceptance for each event]
%
%  - opt.verbose=0; automatic mode, runs the whole detection without user
%                   interaction, only presents a summary figure at the end
%                   (if selected)
%
% inputs:
%   - dat: either string argument with filename incl path
%          eg : dat='E:\data\xyz.ncs'
%          structure with fields
%             dat.env  :   envelope of bandpassed signal of interest
%             dat.dat  :   continuoulsy sampled channel data (raw signal)
%             dat.ts   :   time stamps for each dat sample
%             dat.fs   :   sampling frequency
%             (optional)
%             dat.info :   meta info on data
%
%
%   - opt: structure array with options:
%            see sw_run_spindle.m for default values

% Ullrich Bartsch, University of Bristol, 2010
% v 0.3 : Edited by Julia/Ulli for manual spindle detection. In verbose plus mode, it
% presents putative spindles that pass certain criteria, including length
% and frequency. User is then asked to make a decision whether it is a real
% spindle or something else. 
% v 0.3 : Edited by Julia, verbosePlus mode added 1/6/2016
% v 0.2 : sw version, separate function, cleared up code, event needs to be
%         above detection threshold >50% of its length
% v 0.1 : incl maximum length, options check;added to server: 07/06/13

%--------------------------------------------------------------------------

%% --------------------------------------------------------------------------
% spindle detection
% detect threshold crossings:
%global all_detect;
ylims=[0 45];
yraw=35;
yfilt=20;

% candiadate threshold crossings:
start_ind=find(dat.env(1:end-1)<opt.detectlim & dat.env(2:end)>opt.detectlim);
stop_ind=find(dat.env(1:end-1)>opt.detectlim & dat.env(2:end)<opt.detectlim);

% ignore starts without matching stops 
while stop_ind(1)< start_ind(1)
    stop_ind(1)=[];
end
while start_ind(end)>stop_ind(end)
    start_ind(end)=[];
end


% ignore events within noise epochs:
if ~isempty(opt.noise)
    %isnoise=find((dat.env>opt.noiselim)); 
    noiseind=[opt.noise.ind];
    
    hasnoise1=ismember(start_ind,noiseind);
    hasnoise2=ismember(stop_ind,noiseind);
    
    start_ind(hasnoise1&hasnoise2)=[]; stop_ind(hasnoise1&hasnoise2)=[];
    %disp('    sleepwalker')   
    disp(['   sw > removed ' num2str(length(find(hasnoise1&hasnoise2))) ' spindles within noisy epeochs!'])  
    disp(' ')
end


% walk through each detection in verbose mode:
%opt.verbose=1;
if (opt.verbose || opt.verbosePlus)
    
    fh = figure;
    %clf
    set(fh,'color','w','position',[ 15       252         560         706])
    plot(dat.ts,dat.rz,'k')
    set(gca,'xgrid','on', 'xtick',[round(dat.ts(1)):round(dat.ts(end))])
    hold on
    
    plot(dat.ts,dat.z+yraw,'color',[.5 .5 .5], 'LineWidth',0.1);
    plot(dat.ts,dat.fz+yfilt,'color',[.5 .5 .5]);
    
    %plot(dat.ts,ones(size(dat.ts))*(20+opt.detectlim),'r')
    %plot(dat.ts,ones(size(dat.ts))*(20-opt.detectlim),'r')
    
    plot(dat.ts,dat.env,'-','color','k');
    plot(dat.ts,ones(size(dat.ts))*(opt.detectlim),'r')
    plot(dat.ts,ones(size(dat.ts))*(opt.startendlim),'r--','linewidth',2)
    plot(dat.ts(start_ind),dat.env(start_ind),'go');
    plot(dat.ts(stop_ind),dat.env(stop_ind),'ro');
    
    
    hold off
    xlabel('time (sec)');
    set(gca, 'ylim' ,ylims,'ytick',[0 opt.detectlim])
    %     formatfig
     if opt.verbose
        disp('##########################################################################')
        disp('                   Spindle detection: Verbose mode!'  )
        disp('##########################################################################')
        disp(' ')
        disp('                    Hit any key to step through!')
        disp(' ')
     end
end
%start=dat.ts(startind);
%stop=dat.ts(stopind);


if opt.startind~=1
    i=opt.startind;
    disp (' !!!! ');
    disp (['   sw > startindex set to ' num2str(opt.startind) '!'])
    disp (' !!!! ');
else
    i=1;
end

max_n=length(start_ind);

%progress bar:
if ~(opt.verbose || opt.verbosePlus)
   % mh=waitbar(0,['Found ' num2str(max_n) ' threshold crossings! Computing spindle properties now!']);
    pause(0.25)
    %waitbar(0,mh,'Press Ctrl-C to interrupt. Set opt.verbose=1 to see individual detctions.' );
    progressbar(0,2)
end

%tmp=zeros(size(length(start_ind),2));

% for every detection check for surrounding events
j=1;
ispin=1;
%spin=[];

% manualDetectIdx=1;
% all_detect.dat=dat;
% all_detect.opt=opt;


%% main detection loop 
while i<length(start_ind)-1
          
    % set to candidate event 
    if (opt.verbose || opt.verbosePlus)
        fprintf (['   sw > #%g:  tstart %g, tstop %g , length %g \n'] ,i,  dat.ts(start_ind(i)), dat.ts(stop_ind(i)), (dat.ts(stop_ind(i))-dat.ts(start_ind(i))) );
        %disp( ['  length:' num2str(spin_len)  '  gap: ' num2str(spin_gap)  '  amp: '  num2str(spin_mx) ]);
        %hold on
        %plot(dat.ts(start_ind(i):stop_ind(i)),dat.rz(start_ind(i):stop_ind(i)),'y','linewidth',2);
        %hold off
        set(gca,'xlim',[dat.ts(start_ind(i))-opt.pltwin dat.ts(stop_ind(i))+opt.pltwin],'ylim',ylims)
        %         pause(.1) %
        %         formatfig%
    end
    
    ii=0;     %index for jumping events
    detect=0; %set to 1 if we like what we found
    
    % take current preliminary event
    tmp(1)=start_ind(i);
    tmp(2)=stop_ind(i);
    
    % check for length and gap to other events around the current threshold crossing
    dtspin=dat.ts(stop_ind(i))-dat.ts(start_ind(i)); %lentgh
    tgap=dat.ts(start_ind(i+ii+1)) - dat.ts(stop_ind(i+ii)); %gap to next event
    
    % check properties of candiadte event: start with length(duration)
    
    % event is longer than min length
    if ( dtspin>=opt.minlen ) 
        tmp(1)=start_ind(i);
        tmp(2)=stop_ind(i);
        if (opt.verbose || opt.verbosePlus); fprintf( '   %2.2f  > min length  \n' , dtspin); end
        
        while ( (dat.ts(start_ind(i+ii+1))-dat.ts(stop_ind(i+ii)) <= opt.mingap)  && (i+ii+1<length(start_ind)) &&  (i+ii+1 <= max_n) ) % other events close?
            tmp(2)=stop_ind(i+ii+1); % set end index too last downward threshold crossing
            if (opt.verbose || opt.verbosePlus)
                
                %disp([ '         '  num2str([ i+ii+1 start_ind(i+ii+1) stop_ind(i+ii+1)...
                %                                     dat.ts(stop_ind(i+ii+1))-dat.ts(start_ind(i+ii+1)) ...
                %                                     dat.ts(stop_ind(i+ii+1))-dat.ts(start_ind(i+ii+1))  ]) ] )
                fprintf( [ '   sw > %2.2f < min gap \n' ] , dat.ts(start_ind(i+ii+1))-dat.ts(stop_ind(i+ii))  )
                
                set(gca,'xlim',[dat.ts(start_ind(i))-opt.pltwin dat.ts(tmp(2))+opt.pltwin])
                
            end
            
            ii=ii+1;
        end
        
        i=i+ii+1;
        j=j+1;
        detect=1;% sucess
        
    % event is too short but next event is close by    
    elseif (dtspin < opt.minlen) && (tgap <= opt.mingap) && (i+ii+1 <= max_n) 
        if (opt.verbose || opt.verbosePlus) ;  fprintf( [ '   > spindle length < minlen  but %2.2f <  min gap \n' ], tgap); end
        tmp(1)=start_ind(i);
        tmp(2)=stop_ind(i);
        
        while  (dat.ts(start_ind(i+ii+1)) - dat.ts(stop_ind(i+ii)) < opt.mingap) && (i+ii+1 < max_n-2)  % more than one event close by?
            if (opt.verbose || opt.verbosePlus)
                fprintf( [ '   sw > %2.2f < min gap \n' ],dat.ts(start_ind(i+ii+1))-dat.ts(stop_ind(i+ii)) );
                %disp( [ num2str(dat.ts(start_ind(i+ii+1))-dat.ts(stop_ind(i+ii))) '   <  mingap! ' ] )
                %disp([ '         '  num2str([ i+ii+1 ...
                %                              dat.ts(stop_ind(i+ii+1))-dat.ts(start_ind(i+ii+1))...
                %                              dat.ts(start_ind(i+ii+1))-dat.ts(stop_ind(i+ii))]) ] );
            end
            tmp(2)=stop_ind(i+ii+1);
            ii=ii+1;
        end
        
        if (dat.ts(tmp(2)) - dat.ts(tmp(1)))  >= opt.minlen  % is it altogether long enough?
            %disp(['final ' num2str([ j tmp(j,:)]) ] )
            j=j+1;
            i=i+ii+1;
            detect=1;
            
        else
            detect=0;
            %tmp=[];
            i=i+ii+1;
        end
    
    % too short 
    else
        if (opt.verbose || opt.verbosePlus);  disp(['   >  #' num2str(i) ' no detection']); end
        i=i+1;
        detect=0;
    end
    
    
    %calc some preliminary properties
    spin_len=dat.ts(tmp(2)) - dat.ts(tmp(1));
    %spin_gap=dat.ts(start_ind(i+1))-dat.ts(stop_ind(i));
    spin_mx= max( dat.fz(tmp(1) : tmp(2)) )*dat.fzstd;
    spin_mn= min( dat.fz(tmp(1) : tmp(2)) )*dat.fzstd;
    spin_amp=sqrt( (spin_mx - spin_mn) )^2 ;
    
    
    if detect
        nabove= length(find(dat.env(tmp(1):tmp(2))> opt.detectlim-opt.startendlim) );
        totlen=  length( dat.env(tmp(1):tmp(2)) );
        if (opt.verbose || opt.verbosePlus)
            fprintf( '   sw > prelim  detection   len = %2.2f,  max = %2.2f,  min = %2.2f, amp = %2.2f \n', spin_len,spin_mx,spin_mn,spin_amp)
        end
    end
    
    % check if other criteria apply
    if detect ...
            && (spin_amp < opt.maxamp) ...
            && (spin_len > opt.minlen) ...
            && (spin_len < opt.maxlen) ... % && (spin_freq > 6)...
            && (spin_amp > opt.minamp)... % 
            &&  (nabove/totlen) >= 0.5...
            &&  ~any(ismember(tmp(1):tmp(2),[opt.noise.ind])) 
         
        % apply local threshold for start & stop times changed from nsamples to time window  % make into function?
        % we look for a local threshold within the local time window
        nlook=dat.fs*opt.localwin;

        %start time
        if tmp(1)>nlook %make sure we are nlook away from beginning of file

            % start stop crossing or local min
            k1=find( dat.env(tmp(1)-nlook : tmp(1)-1)<opt.startendlim &  dat.env(tmp(1)-(nlook-1):tmp(1))>opt.startendlim, 1,'last' );
        else
            k1=[];
        end

        if ~isempty(k1)
            tmp2(1)=tmp(1)-(nlook-max(k1));
        else
            tmp2(1)=tmp(1);
        end


        % stop time
        if tmp(2)<length(dat.env)-nlook
            k2=find( dat.env(tmp(2):tmp(2)+nlook-1)>opt.startendlim &  dat.env(tmp(2)+1:tmp(2)+nlook)<opt.startendlim ,1,'first');
        else
            k2=[];
        end

        if ~isempty(k2)
            tmp2(2)=min(k2)-1+tmp(2);
        else
            tmp2(2)=tmp(2);
        end

        % closest local minimum?
        %     [m,k1]=findpeaks( -dat.env(tmp(jj,1)-100:tmp(jj,1)));
        %     [m,k2]=findpeaks( -dat.env(tmp(jj,2):tmp(jj,2)+100));
        %
        %     tmp2(jj,1)=tmp(jj,1)-(100-max(k1));
        %     tmp2(jj,2)=min(k2)-1+tmp(jj,2);
        %disp([k1 k2]);

        % check if new start/stop times do not include other detections
        if detect && exist('spin','var')... 
            && any (dat.ts(tmp(1)) > [spin.tsstart] &  dat.ts(tmp(1)) < [spin.tsstop] )

            t0=dat.ts(tmp(1));
            t1=dat.ts(tmp(2));

            tmp_start=[spin.tsstart] ;
            tmp_stop=[spin.tsstop] ;

            disp('   sw > spindle overlap: merging start and stop times')
            tmp(1)=find(dat.ts>=tmp_start(end),1,'first');



            ispin=ispin-1;
            %detect=0;
        end




        %% get final event properties!!
        % all local maxima & minima
        istart=tmp2(1);
        istop=tmp2(2);

        [mx0,mxk0]=findpeaks(  dat.f(tmp2(1):tmp2(2)) );
        [mn0,mnk0]=findpeaks( -dat.f(tmp2(1):tmp2(2)) );

        indallmax=tmp2(1)+mxk0-1;
        indallmin=tmp2(1)+mnk0-1;

        [~,mmxk]=max(mx0);
        [~,mmnk]=max(mn0);

        % absoulute local maximum & minimum
        indabsmax=tmp2(1)+mxk0(mmxk)-1;
        indabsmin=tmp2(1)+mnk0(mmnk)-1;

    
        spin(ispin).stage='';
        spin(ispin).tsstart=dat.ts(tmp2(1));
        spin(ispin).tsstop=dat.ts(tmp2(2));
        
        spin(ispin).tsabsmax=dat.ts(indabsmax);
        spin(ispin).tsabsmin=dat.ts(indabsmin);
        
        %       add behav score to spindle props?
        %         if ~isempty(eventid)
        %             spindle(jj).state=eventid(indabsmax);
        %         end
        
        spin(ispin).indabsmax=indabsmax;
        spin(ispin).indabsmin=indabsmin;
        
        spin(ispin).tsallmax=dat.ts(indallmax);
        spin(ispin).tsallmin=dat.ts(indallmin);
        
        spin(ispin).amplitude=abs (dat.f(indabsmax))+ abs( dat.f(indabsmin));
        spin(ispin).length=dat.ts(tmp(2))-dat.ts(tmp(1));
        %spindle(jj).freq=length(indallmax)/spindle(jj).length;
        
        spin(ispin).freq=1/mean(diff(dat.ts(indallmax)));
        
        if isfield(dat,'scoring')
             k= knnsearch(dat.scoring.epochlimits', spin(ispin).tsabsmax - (dat.scoring.epoch/2) );
             spin(ispin).stage = dat.scoring.stage{k};
        end   
        
        if opt.textoutput
      
            fprintf ('   sw > %g spindle:   ts %g, amp=%g, f=%g, len=%g \n',...
            ispin, dat.ts(tmp2(1)), spin(ispin).amplitude, spin(ispin).freq, spin(ispin).length );
        end
        
        %spindle(jj).instamp=abs (dat.hil(spindle(jj).indstart : spindle(jj).indstop));
        %spindle(jj).instfreq=(dat.instf(spindle(jj).indstart : spindle(jj).indstop));
        
        
        if (opt.verbose || opt.verbosePlus) %&& (spin_len > opt.minlen) %will not show you very short ones
            set(0,'CurrentFigure',fh)
            hold on
            
            text(dat.ts(start_ind(i-1)), 44, ['#' num2str(i-1)] );
            text(dat.ts(start_ind(i-ii-1)), 44, ['#' num2str(i-ii-1)] );
            text(dat.ts(istop), 43, ['#' num2str(ispin)],'color',[.2 .6 .2] );
            
            axis ([dat.ts(tmp2(1))-opt.pltwin,dat.ts(tmp2(2))+opt.pltwin, ylims])

            plot(dat.ts(tmp2(1):tmp2(2)), dat.rz(tmp2(1):tmp2(2)) ,'b');
            plot(dat.ts(tmp2(1)),dat.env(tmp2(1)),'g+');
            plot(dat.ts(tmp2(2)),dat.env(tmp2(2)),'r+');

            plot(dat.ts(indallmax),dat.fz(indallmax)+yfilt,'yO');
            plot(dat.ts(indallmin),dat.fz(indallmin)+yfilt,'mO');

            plot(dat.ts(indabsmax),dat.fz(indabsmax)+yfilt,'r+');
            plot(dat.ts(indabsmin),dat.fz(indabsmin)+yfilt,'b+');

            plot( dat.ts( tmp2(1):tmp2(2) ) ,dat.z  ( tmp2(1):tmp2(2) )+yraw,'b');
            plot( dat.ts( tmp2(1):tmp2(2) ), dat.fz( tmp2(1):tmp2(2) )+yfilt,'b');

            hold off;
%             disp(' ')
%             fprintf ('  > %g spindle:   ts %g, amp=%g, f=%g, len=%g \n',...
%                 ispin, dat.ts(tmp2(1)), spindle(ispin).amplitude, spindle(ispin).freq, spindle(ispin).length );

            %disp(['final ' num2str([ jj tmp2]) ] )

%             formatfig
            %             if ~isempty(eventid)
            %                 th=text(spindle(jj).tsstart-(opt.pltwin)+0.1,40,stages{eventid(tmp2(1))},'fontsize',20);
            %             end
                %verbosePlus Mode Feedback
 
    
       
       % final manual check on detected events only
       
        pause
        if opt.verbosePlus %verbosePlus == 1
            str='0';
            
            while strcmp(str, '0')
                disp('   sw > Manual event check:') 
                prompt = '   sw > Select event type: seizure = z. spindle = s. noise = n. other = o. ';
                str = input(prompt,'s');
               
                %If user presses enter without entering a character,
                %str will be empty, causing the while loop to fail. So if
                %str is empty, put a dummy character to make the code
                %through the loop again
                if(isempty(str) || length(str)>=2)
                    str='0';
                end

                switch str
                    case 'z'
                        man(ispin).seizure = 1;
                        man(ispin).spindle = 0;
                        man(ispin).noise= 0;
                        man(ispin).other = 0;                      
                        disp('    >> seizure')
                    case 's'
                        man(ispin).seizure = 0;
                        man(ispin).spindle = 1;
                        man(ispin).noise= 0;
                        man(ispin).other = 0;
                        disp('    >> spindle')
                    case 'n'
                        man(ispin).seizure = 0;
                        man(ispin).spindle = 0;
                        man(ispin).noise= 1;
                        man(ispin).other = 0;
                        disp('    >> noise')
                    case 'o'
                        man(ispin).seizure = 0;
                        man(ispin).spindle = 0;
                        man(ispin).noise= 0;
                        man(ispin).other=1;
                        disp('    >> other')
                    otherwise
                        fprintf('   sw > Invalid answer! Score again...\n');
                        str='0';
                        
                     
                end
                fprintf('   \n'); 
               pause(0.5)
               %pause
            end
            %man.start=man.start+1;
            man(ispin).stage='';
            man(ispin).tsstart=dat.ts(istart);
            man(ispin).tsstop=dat.ts(istop);
            man(ispin).tsabsmax=dat.ts(indabsmax);
            man(ispin).tsabsmin=dat.ts(indabsmin);
            man(ispin).indabsmax=indabsmax;
            man(ispin).indabsmin=indabsmin;
            man(ispin).tsallmax=dat.ts(indallmax);
            man(ispin).tsallmin=dat.ts(indallmin);
            man(ispin).amplitude=abs (dat.f(indabsmax))+ abs( dat.f(indabsmin));
            man(ispin).length=dat.ts(istop)-dat.ts(istart);
            man(ispin).freq=1/mean(diff(dat.ts(indallmax)));
            
            if isfield(dat,'scoring')
                k= knnsearch(dat.scoring.epochlimits', man(ispin).tsabsmax - (dat.scoring.epoch/2) );
            	man(ispin).stage = dat.scoring.stage{k};
            end
          
          

        end
    
    
       % delete(th)
        end
        
       
        ispin=ispin+1;
        
        
        
    else % no detect
        if opt.verbose
            set(0,'CurrentFigure',fh)
            hold on
            
            set(gca,'xlim',[dat.ts(tmp(1))-opt.pltwin  dat.ts(tmp(2))+opt.pltwin])
            %plot( dat.ts( tmp(1):tmp(2)) ,dat.z  ( tmp(1):tmp(2) ) +30,'r');
            %plot( dat.ts( tmp(1):tmp(2)) , dat.fz( tmp(1):tmp(2) ) +20,'r');
            plot(dat.ts(tmp(1):tmp(2)), dat.rz(tmp(1):tmp(2)) ,'r');
            hold off
            disp(['   sw > criteria not met! '])
            
       
            pause
            
            
        end
    end
  
    


    if ~(opt.verbose || opt.verbosePlus)
        %mh=waitbar(i/max_n,mh,['Checked ' num2str(i) ' of ' num2str(max_n) ' threshold crossings!  Press Ctrl-C to interrupt.']);
        progressbar(i/(max_n+20),2)

    end

    if (opt.verbose || opt.verbosePlus)
        disp(' ')
    end
    
end %while


if ~(opt.verbose)
    
    if ~exist('var')
        spin=[];
        disp(['   sw > No spindles detected ! Check settings and or data!'])
    else
        disp(['   sw > Detected ' num2str(ispin) ' spindles out of ' num2str(max_n) ' threshold crossings!'])
        
    end
    % waitbar(i/max_n,mh)
    progressbar(1,2)
    pause(2)
    close all hidden
%close (mh)
end     

if opt.verbosePlus
    spin0.man=man;
    spin0.auto=spin;
    spin=spin0;
end

end





