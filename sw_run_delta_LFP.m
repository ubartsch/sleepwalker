function [opt,delta,stats,fh] = sw_run_delta_LFP(varargin)
% [rip] = run_ripple_LFP(dat, opt)
% top level function to perform ripple detection 
% inputs: 
%        1) dat -  data input which can be either
%               a) string dat='C:/lfpdata/rec001.ncs' or
%               b) data structure with the following necessary fields:
%                   dat.csc, dat.ts, dat.fs
%        2) opt -  options structure with options for detection routine, see
%                 default values below.


%% check inputs
fprintf ('   sleep walker slow/delta wave detection \n\n'  )


if nargin<1
   eval  ( [ 'help run_delta_LFP' ]) 
   error ( 'sleepwalker error : no arguments for run_delta_LFP! \n   Check the .m-file header for help!' )
   
elseif nargin<2
    dat=varargin{1};
    opt=struct;
    
elseif nargin==2
    dat=varargin{1};
    opt=varargin{2};
    
else
    eval  ( [ 'help run_delta_LFP' ]) 
    error ( ' sleepwalker error: wrong number of arguments for run_delta_LFP! \n   Check the .m-file header for help!')  
end




    
    
%% default values
%check options  set to standard values if not set:
opt0  =     {'detect',   'delta';     % detect slow wave/delta events
             'signaltype', 'lfp';  
            'verbose',   0;           % verbose on|off [0|1]; eg: opt.verbose=0;
            'verbosePlus',   0;           % verbose on|off [0|1]; eg: opt.verbose=0;
            'again',     0;           % do data filtering, rectifcation and zscoring again
            'load',      1;           % load previously filtered/rectified raw data? [0|1]; eg opt.load=1;
            'save',      1;           % save filtered/rectified to speed up repeated analysis? [0|1]; eg opt.load=1;
            'reFs',      100;         % sampling rate to downsample to;  eg opt.reFs=500;
            'filetype',  'ncs';       % data type
            'filterband',[0.5 4];     % bandpass filter edges
            'plt',       1;           % generate summary figure and print pdf [0|1]
            'pltwin' ,    1;
            'startind',  1;
            'localwin',  0.5;         % window to find beginning and end of event with local threshold
            'minlen',    0.25;        % min length of slow wave/delta in sec
            'maxlen',    3;           % max length of slow wave/delta in sec
            'mingap',    0.5;           % min gap to seperate slow wave/delta
            'minamp',    50;          % min amplitude for slow wave/delta events in uV
            'maxamp',    1500;        % max amplitude for slow wave/delta events in uV
            'detectlim', 3.0;         % detection limit in signal sd
            'startendlim', 1;       % start/end limit for start/end times in signal sd
            'noiselim',   35;         % exclude all signal with sd >= noiselim  
            'clipping',  [-inf inf];  % raw uV value that indicates clipping ( = max range )
            'smoothenv', [0 0];       % apply moving_average to power envelope? yes|no nsamples [0|1 10]
            'ts',        [];          % limit data to ts range 
            'lenbin', 0:0.1:3;
            'ampbin', 0:25:1000;
            'freqbin', [0:0.2:5]};        
                 
for i=1:length(opt0)
    if ~(isfield(opt ,opt0{i,1}))
        opt.( opt0{i,1} ) = opt0{i,2};
        disp([ '   > field opt.' opt0{i,1} ' not set! Using standard value: ' num2str(opt0{i,2})])
    end
end
% some other settings:
pltwin=0.2;    % size of the plotting window in verbose mode
locthrwin=0.1; %  local threshold window


%% load data
datfields={'csc','ts','fs'};
if  ischar(dat)
    % string input : load from file
    [opt.pat, opt.fn, opt.ext] = fileparts(dat);
    opt.pat=[opt.pat '\'];
    [dat] = sw_load_LFP(opt) ;    
else 
    % direct data input: check struct with fields csc, ts, fs
    for i=1:length(datfields)
        if ~isfield(dat,datfields{i})
            error( [ '  > dat is missing field ' datfields{i} ])
        else
            if isempty(dat.(datfields{i}))
                error( ['  > dat.' datfields{i} ' is empty!' ])
            end
        end
    end
    
    if ~isfield(opt,'pat')
          
        opt.pat = [pwd '\'];
        opt.fn  = '';
        opt.ext = '';
        disp ('  > using working directory to save results!' )
    end
    
end

%% 

%% filter data
dat= sw_filter_lfp (dat, opt);

%% downsample data
[dat.csc,dat.ts,dat.fs]=sw_dsample_lfp(dat,opt.reFs);

%% simple noise rejection
manualnoisecheck=0;
% [opt.noise]=sw_simple_noise_rejection(dat,opt.noiselim, 0);
[opt.noise,dat]=sw_simple_noise_rejection(dat,opt.noiselim, manualnoisecheck);

%% filter & envelope
[dat,opt]= sw_get_env(dat,opt);

%% run detections
[delta]=sw_detect_delta(dat,opt);

%% basic analysis 
[stats]=sw_get_stats(dat,delta,opt);

if opt.plt
%% mk summary figure
    [fh]= sw_plot_detection(dat,delta, stats,opt);
else
    fh=[];
end

ext = '.mat';
if isfield (opt,'suffix') && ~isempty(opt.suffix)
  suf = opt.suffix;
else
  suf ='';
end


if isfield (dat,'ch_list') && ~isempty(dat.ch_list)
    svfn=[opt.pat opt.fn '_' dat.ch_list{1} '_' opt.detect '_results' ];
else
    svfn=[opt.pat opt.fn '_' opt.detect '_results' ];
end

%save
save([svfn suf ext] , 'delta','opt','stats')
fprintf ('   > saved  %s \n\n' , [ svfn suf ext])


%save matlab figure?
%savefig(fh,[svfn(1:end-4) '.fig'])

%rem temp data?
%[out] = sw_rem_temporary_data(opt);

