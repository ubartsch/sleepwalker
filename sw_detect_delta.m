function [delta]=sw_detect_delta(dat,opt)
% function to detect delta waves in lfp:
% inputs: 
%   - fn0: string argument with filename incl path
%       eg : fn0='E:\data\xyz.ncs'
%   - opt: structure array with options:
%             opt.filetype: filetype of raw data file:
%                   can be: 'ncs'  - cheetah (default)
%                           'mat'  - ML files from spike2, Keith
%                           'abf2' - abf2 file Randall lab   
%                  eg > opt.filetype='ncs';
%             opt.verbose: activate verbose mode, plot every single slow
%             wave
%             detection + walk through
%                  eg > opt.verbose=1;
%
%             opt.minlen: min length of a spindle in seconds
%                  eg > opt.minlen=0.1;   
%
%             opt.mingap: minimum gap for spindles to be considered separate events
%                  eg > opt.mingap=0.010; 
%
%             opt.detectlim: detection limit in signal STD
%                  eg > opt.detectlim=4;
%
%             opt.startendlim: local limit for for start end times in signal STD                            
%                   eg >  opt.startendlim=2; 
%             
%             opt.load: if the script has run once before, it will save
%             the filtered downsampled trace in the source directory. set
%             this option to 1 for loading the previously filtered and
%             downsampled trace, 0 for using the original data
%                   eg > opt.load=1;
%
%             opt.reFs: target sampling Frequency for resampling
%                   eg > opt.reFs=600: 
%
%
%             opt.plt: set to 1 for displaying summary plot of results
%                   eg > opt.plt=1;
       
% 0.2 Feb 2015 : sw version, beta
% 0.1 Nov 2010 alpha
% Ullrich Bartsch, University of Bristol
%--------------------------------------------------------------------------
cols=['bbgyyr'];

%to do:
% extract slope from waveforms, set artefact detection levels
% add default settings
% check input options
% change tmp(x) to meaningfiul var names!
% change hard coded axis limits to vars

%--------------------------------------------------------------------------
% delta detection

% find threshold crossings:
neg_ind=find(dat.env(1:end-1)>-opt.detectlim & dat.env(2:end)<-opt.detectlim);% find neg half waves
pos_ind=find(dat.env(1:end-1)<opt.detectlim & dat.env(2:end)>opt.detectlim);  % find pos half waves


% generate combined index vector and mark pos neg threshold crossings
allind=[pos_ind neg_ind];
posneg=[ ones(1, length(pos_ind)) -ones(1,length(neg_ind)) ];

[allind, srti]=sort(allind);
posneg=posneg(srti);

%if stop_ind(1)< start_ind(1); stop_ind(1)=[]; end;
%opt.verbose=1;
if opt.verbose
    
    close all hidden
    % plot all data and candiate crossings in one figure
    figure(1)
    clf
    set(gcf,'color','w','position',[ 15       252         560         706])
    
    plot(dat.ts,dat.env,'k')
    set(gca,'xgrid','on', 'xtick',[round(dat.ts(1)):round(dat.ts(end))])
    hold on
    
%     % maybe spikes in stuct S from MClust
%     if ~isempty(S)
%     for k=1:length(S)
%         %S0 = S(ii).ts( S(ii).ts>opt.ts(1)/1e6 & S(ii).ts< opt.ts(2)/1e6 );
%         plot(S(k).ts,ones(length(S(k).ts) ,1)+15+k/2, '.k');
%     end
%     end
%     

    %plot(stsbin,zscore(c)+15,'g');
    plot(dat.ts,dat.z+10,'color',[.5 .5 .5]);
    kc=ismember(dat.csc ,opt.clipping);
    plot(dat.ts(kc),dat.z(kc)+10,'.', 'color',[.9 .5 .5]);
    
    kind=[opt.noise.ind];
    plot(dat.ts(kind),dat.z(kind)+10,'.', 'color',[.3 .3 .3]);
    
    %plot(dat.ts,dat.fz+20,'color',[.7 .7 .7]);
    plot(dat.ts,ones(size(dat.ts))*opt.detectlim,'r')
    plot(dat.ts,-ones(size(dat.ts))*opt.detectlim,'r')
    
    plot(dat.ts,ones(size(dat.ts))*opt.startendlim,'r--')
    plot(dat.ts,-ones(size(dat.ts))*opt.startendlim,'r--')
    
    
    %plot(dat.ts,dat.env,'r.');
    plot(dat.ts(allind),dat.env(allind),'bo');
    %plot(dat.ts(stop_ind),dat.fz(stop_ind),'bo');
    
    %formatfig
    hold off
    disp('##########################################################################')
    disp('                 Delta wave detection: Verbose mode!'  )
    disp('##########################################################################')
    disp(' ')
    disp('                    Hit any key to step through!')
    disp(' ')
else
%      msg0={''; ' Starting delta wave detection... ' ;'';'';''} ;
%      mh = waitbar(0,msg0,'name', 'Delta wave detection 0.2 beta');
end

%progress bar:
if ~(opt.verbose || opt.verbosePlus)
   % mh=waitbar(0,['Found ' num2str(max_n) ' threshold crossings! Computing spindle properties now!']);
    pause(0.25)
    %waitbar(0,mh,'Press Ctrl-C to interrupt. Set opt.verbose=1 to see individual detctions.' );
    progressbar(0,2)
end

j=1;
i=1;


if opt.verbose && isfield(opt,'startind')
    %j=opt.startind;
    i=opt.startind;
end

max_n=length(allind);
%tmp=zeros(size(length(allind),2));
if ~opt.verbose
    msg={ ['   sw > Found ' num2str(max_n) ' threshold crossings!'],...
          '   sw > Computing delta wave properties now!',...
          '   sw > Press Ctrl-C to interrupt.' };
           
    %waitbar(0,mh,msg);
    fprintf('%s\n',msg{1});
    fprintf('%s\n',msg{2});
    fprintf('%s\n',msg{3});
    
    progressbar(0,3)
    
    
end

win0=round(dat.fs*1); %start detection window
win1=dat.fs*opt.pltwin; %ceil(dat.fs*0.5); %
winchk=round(opt.mingap * dat.fs);  %round(dat.fs*opt.mingap);

%%fprintf (' detect delta:  mingap = %d\n' , winchk );


delta=[];

while i<length(allind)-1 % walk through all detection indices
    
    if opt.verbose 
        fprintf ('%s', 'checking threshold crossing: ')
        fprintf ('%d\n' , i );
       % fprintf ('%\b');

    end
    
          
     %%  check for neighbouring crossings in a window of opt.mingap ahead
     k0=0;        
     ii=i;
     while ~isempty(k0) 
         %advance window
          k0=find(allind > allind(ii) & allind <= allind(ii)+winchk ,1,'first');
          if ~isempty(k0)
                if opt.verbose
                    disp('next event closer than mingap!')
                    hold on
                    plot(dat.ts(allind(k0)),dat.env(allind(k0)),'m*');
                    if allind(ii)-winchk>1
                         axis ([dat.ts(allind(i)-winchk) dat.ts(allind(k0)+winchk) -10 20])
                    else
                         axis ([dat.ts(1) dat.ts(allind(ii)+winchk) -10 20])
                    end
                    hold off
                end
                ii=ii+1;
                warning off all
                      
          end        
     end
    
  
    %% determine type of detected delta waves
    
    if  ii>i+1 %&& length(kp)>=2 ;     %delta wave episode 
            % disp('prelim  episode')
            tmp(1)=allind(i);     %tmp(1) =! ind0 
            ind0=allind(i);
            episode_thresh=[allind(i:ii)];
            episode_posneg=[posneg(i:ii)];
            wavetype=3;
            i=ii+1;
          
    elseif ii==i+1 %&& length(kp)<2    %full wave
            % type
            % disp('prelim  full')
            wavetype=[posneg(i) - posneg(ii) ];
            tmp(1)=allind(i);
            tmp(2)=allind(ii);
            i=i+2;
               
    elseif ii==i   %half wave
        % disp('prelim  half')
        wavetype=[posneg(i)]; 
        tmp(1)=allind(i);
        i=i+1;   
        
    else
        disp('uncategorised!')
        i=i+1; 
    end

    
%     % skip detection if index is in noise
%     if  ~any(ismember( tmp , [opt.noise.ind] ) )
        
    %% get start, max/min and end times
    if  (tmp(1)-(win0*3) > 0) && (tmp(1)+(win0*3) < length(dat.env) )
        switch wavetype
            
            case -1  %% negative half-wave
                tmp(3)=next_peak( dat.env(tmp(1)-win0:tmp(1)),tmp(1),'maxback'); % start 
                tmp(4)=next_peak( dat.env(tmp(1):tmp(1)+win0),tmp(1),'minfwd');  % min 
                tmp(6)=next_peak( dat.env(tmp(1):tmp(1)+win0),tmp(1) ,'maxfwd'); %end

                % calculate criteria 
                % amplitude:start to min
                amp= sqrt(  ( dat.env(tmp(4)) - dat.env(tmp(3)) )^2 )*dat.fzstd;
                dt= dat.ts(tmp(4))-dat.ts(tmp(3)); 
                len=dat.ts(tmp(6))-dat.ts(tmp(3));
                maxval=max (abs ( [max(dat.csc(tmp(3):tmp(6))) min(dat.csc(tmp(3):tmp(6))) ]) ) ;


               [maxsl,maxsli]=max_slope(dat.env(tmp(3):tmp(4))',dat.ts(tmp(3):tmp(4)),tmp(3),'down');

               %add length crit for all cases? 


            case 1   %% positive half-wave        
                tmp(3)=next_peak( dat.env(tmp(1)-win0:tmp(1)),tmp(1),'minback'); % start 
                tmp(4)=next_peak( dat.env(tmp(1):tmp(1)+win0),tmp(1),'maxfwd');  % max
                tmp(6)=next_peak( dat.env(tmp(1):tmp(1)+win0),tmp(1) ,'minfwd'); %end
                dt= dat.ts(tmp(4))-dat.ts(tmp(3)); 
                len=dat.ts(tmp(6))-dat.ts(tmp(3));
                amp= sqrt(  ( dat.env(tmp(4)) - dat.env(tmp(3)) )^2 ) *dat.fzstd;
                maxval=max (abs ([ max(dat.csc(tmp(3):tmp(6))) min(dat.csc(tmp(3):tmp(6)))] ) );

    %             dVdt=diff(dat.env(tmp(3):tmp(4))) /diff(dat.ts(tmp(3):tmp(4)));
    %             [maxsl ,maxsli]=max(dVdt);maxsli=tmp(3)+maxsli-1;

               [maxsl,maxsli]=max_slope(dat.env(tmp(3):tmp(4))',dat.ts(tmp(3):tmp(4)),tmp(3),'up');


            case -2  %% negative-positive full-wave    
                tmp(3)=next_peak( dat.env(tmp(1)-win0:tmp(1)),tmp(1),'maxback'); % start 
                tmp(4)=next_peak( dat.env(tmp(1):tmp(1)+win0),tmp(1),'minfwd');  % min 

                tmp(5)=next_peak( dat.env(tmp(2):tmp(2)+win0),tmp(2),'maxfwd');  % max
                tmp(6)=next_peak( dat.env(tmp(2):tmp(2)+win0),tmp(2) ,'minfwd'); % end

                amp0= sqrt(  ( dat.env(tmp(4)) - dat.env(tmp(3)) )^2 )*dat.fzstd;% amp start - min
                amp= sqrt(  ( dat.env(tmp(4)) - dat.env(tmp(5)) )^2 )*dat.fzstd; % amp min - max
                amp2= sqrt(  ( dat.env(tmp(5)) - dat.env(tmp(6)) )^2 )*dat.fzstd;% amp max - end

                len=dat.ts(tmp(6))-dat.ts(tmp(3));
                dt= dat.ts(tmp(5))-dat.ts(tmp(4)); 
                maxval=max (abs ([ max(dat.csc(tmp(3):tmp(6))) min(dat.csc(tmp(3):tmp(6)))]) );

                [maxsl,maxsli]=max_slope(dat.env(tmp(4):tmp(5))',dat.ts(tmp(4):tmp(5)),tmp(4),'up');
            case 2  %% positive-negative full-wave    

                tmp(3)=next_peak( dat.env(tmp(1)-win0:tmp(1)),tmp(1),'minback'); % start 
                tmp(4)=next_peak( dat.env(tmp(1):tmp(1)+win0),tmp(1),'maxfwd');  % max

                tmp(5)=next_peak( dat.env(tmp(2):tmp(2)+win0),tmp(2),'minfwd');  % min
                tmp(6)=next_peak( dat.env(tmp(2):tmp(2)+win0),tmp(2) ,'maxfwd'); %end

                amp0= sqrt(  ( dat.env(tmp(4)) - dat.env(tmp(3)) )^2 )*dat.fzstd;
                amp= sqrt(  ( dat.env(tmp(5)) - dat.env(tmp(4)) )^2 )*dat.fzstd;
                amp2= sqrt(  ( dat.env(tmp(5)) - dat.env(tmp(6)) )^2 )*dat.fzstd;
                
                len=dat.ts(tmp(6))-dat.ts(tmp(3));
                maxval=max (abs ( [max(dat.csc(tmp(3):tmp(6))) min(dat.csc(tmp(3):tmp(6))) ]) );

                [maxsl,maxsli]=max_slope(dat.env(tmp(4):tmp(5))',dat.ts(tmp(4):tmp(5)),tmp(4),'down');

                dt= dat.ts(tmp(5))-dat.ts(tmp(4)); 

            case 0 %% douple dip: negative negative or positive positive
                %ignore
                amp=0;
                len=0;

            case 3 %%delta wave episode
                %find start,end, peaks in between

                startthresh=min(episode_thresh);  
                %episode_posneg
                endthresh=max(episode_thresh);
                
               % get start lower threshold crossing
                switch episode_posneg(1)
                    case -1
                        episode_start=next_peak( dat.env(startthresh-win0:startthresh ),startthresh,'maxback'); % start 
                    case 1
                        episode_start=next_peak( dat.env(startthresh-win0:startthresh ),startthresh ,'minback'); % start 
                end
                tmp(3)=episode_start;

                
                %get end lower threshold crossing
                % check total lower threshold window is extendign beyond
                % data end
                kend = endthresh+win0;
                if kend>length(dat.env)
                     kend = length(dat.env);
                end
                
                
                if kend <= length(dat.env)-win0
                    switch episode_posneg(end)
                        case -1
                            episode_end=next_peak( dat.env(endthresh:kend ),endthresh ,'maxfwd'); % end 
                        case 1
                            episode_end=next_peak( dat.env(endthresh:kend ),endthresh ,'minfwd'); % end 
                    end
                    
                else
                   
                    episode_end = length(dat.env)-win0;
                end
                tmp(6)= episode_end;
                
                len=dat.ts(episode_end)-dat.ts(episode_start);
                %disp([dat.ts(episode_start) dat.ts(episode_end)]);
                
                % absmaxval=max (abs ( [max(dat.csc(episode_start:episode_end)) min(dat.csc(episode_start:episode_end)) ]) );

                [ma,kma]=findpeaks(dat.env(episode_start:episode_end)); %max
                %if double peak pick 
                
                [mi,kmi]=findpeaks(-dat.env(episode_start:episode_end));%min 
                kpn=[ ones(size(kma)) (ones(size(kmi))-1) ];

                peaks=[ma mi];
                km=[kma kmi];

                [km,si]=sort(km);
                kpn=kpn(si);
                peaks=peaks(si);

                % max pos & neg amplitude
                [~,mai]=max(ma);
                [~,mii]=max(mi);

                episode_mmax=episode_start+kma(mai)-1;
                episode_mmin=episode_start+kmi(mii)-1;

                episode_allpeaks=episode_start+km-1;
                episode_posneg=episode_start+kpn-1;

                km2=episode_start +km -1;
                 % abs max
                [~,mma]=max(peaks);
                %disp([mma kpn(mma) ]);
                %disp(km)
                if mma==1
                    amp=sqrt(  ( dat.env(episode_start) - dat.env(km2(mma))).^2 )*dat.fzstd;
                    indam(1)=episode_start;
                    indam(2)=km2(mma);
                elseif mma==length(km2)
                    amp=sqrt(  ( dat.env(km2(mma)) - dat.env(episode_end)).^2 )*dat.fzstd; 
                    indam(1)=episode_end;
                    indam(2)=km2(mma);
                elseif  mma<length(km2) && kpn(mma)==1
                    amp=sqrt(  ( dat.env(km2(mma)) - dat.env(km2(mma+1))).^2 )*dat.fzstd; 
                    indam(1)=km2(mma);
                    indam(2)=km2(mma+ 1);
                else
                    amp=sqrt(  ( dat.env(km2(mma-1)) - dat.env(km2(mma))).^2 )*dat.fzstd; 
                    indam(2)=km2(mma-1);
                    indam(1)=km2(mma);
                end
                dtall=[]; ampall=[];
                for ai=1:length(km2)-1
                    dtall(ai)=  dat.ts(episode_allpeaks(ai+1))-dat.ts(episode_allpeaks(ai)); 
                    ampall(ai)= sqrt(  ( dat.env(episode_allpeaks(ai+1)) - dat.env(episode_allpeaks(ai))) ^2 )*dat.fzstd;
                end
                dtm=mean(dtall);
                ampm=mean(ampall);

        end
    else 
        amp=0;len=0;


    end


    if opt.verbose && ( tmp(1)-win0>0 ) && (tmp(1)-(win0*3) > 0) && (tmp(1)+(win0*3) < length(dat.env) )
        %disp(['check threshold crossing: ' num2str(i) ]);
        hold on 
        if abs(wavetype)==1 
         
            %disp([dat.ts(allind(i)) ])
            for k=[ 3 4 6]
                plot(dat.ts(tmp(k)),dat.env(tmp(k)),[cols(k) 'o']);
            end
            plot(dat.ts(tmp(3):tmp(6)),dat.env(tmp(3):tmp(6)),'r');
            plot(dat.ts(maxsli),dat.env(maxsli),'gx');
            axis ([dat.ts(tmp(3))-win1/dat.fs dat.ts(tmp(6))+win1/dat.fs -10 20])
            text(dat.ts(tmp(3)), 15, num2str(i)) ;
               
            fprintf ('  >  half wave %g:       ts %g,  amp %g,  f %g  len %g  slope %g\n', wavetype ,dat.ts(tmp(3)), amp, 0.5/dt, len, maxsl);
        elseif abs(wavetype)==2
          

            for k=2:6
                plot(dat.ts(tmp(k)),dat.env(tmp(k)),[cols(k) 'o']);
            end
            plot(dat.ts(tmp(3):tmp(6)),dat.env(tmp(3):tmp(6)),'r');
            plot(dat.ts(maxsli),dat.env(maxsli),'gx');
            axis ([dat.ts(tmp(3))-win1/dat.fs dat.ts(tmp(6))+win1/dat.fs -10 20])
            
            text(dat.ts(tmp(3)), 15, num2str(i));
            fprintf ('  >  full wave %g:      ts %g,  amp %g,  f %g  len %g  \n',  wavetype ,dat.ts(tmp(3)), amp, 0.5/dt, len);
            
        elseif abs(wavetype)==3

             plot(dat.ts(episode_start:episode_end),dat.env(episode_start:episode_end),'r');
             plot(dat.ts(episode_allpeaks),dat.env(episode_allpeaks),'yo');
             plot(dat.ts(indam(1)), dat.env(indam(1)),'>g')
             plot(dat.ts(indam(2)), dat.env(indam(2)),'<g')
             plot(dat.ts(episode_mmax),dat.env(episode_mmax),'g+');
             plot(dat.ts(episode_mmin),dat.env(episode_mmin),'g+');

             axis ([dat.ts(episode_start)-win1/dat.fs dat.ts(episode_end)+win1/dat.fs -10 20])
              text(dat.ts(tmp(3)), 15, num2str(i)); 
              
             fprintf ('  >  delta episode:   ts %g,  amp %g,  f %g  len %g  \n', dat.ts(episode_start), amp,0.5/dtm, len);
        end
        hold off
    end 
      
   %end
    
   % check if criteria apply
   % added minlen 13/08/2012, UB
   
    test_props= [ (amp>=opt.minamp) (amp<=opt.maxamp) ...
                  (len>=opt.minlen) (len<=opt.maxlen) ( tmp(1)-win0>0 )...
                  ~any( ismember( tmp , [opt.noise.ind])) ];
                  
    if all(test_props)
        
    
       delta(j).stage='';
        
        switch abs(wavetype)
            case {1,2}
                delta(j).type=wavetype;
                delta(j).freq=0.5/dt;
                if isnan (delta(j).freq) ; disp([j dt]); pause; end
                delta(j).amplitude=amp;
                delta(j).length=dat.ts(tmp(6))- dat.ts(tmp(3));
                
                delta(j).maxslope=maxsl;
                
                delta(j).indthresh=tmp(1);
                delta(j).indstart=tmp(3);
                delta(j).indpeak=tmp(4);
                delta(j).indstop=tmp(6);
                                
                delta(j).tsthresh=dat.ts(tmp(1));
                delta(j).tsstart=dat.ts(tmp(3));
                delta(j).tsstop=dat.ts(tmp(6));
                delta(j).tsabsmax=dat.ts(tmp(4));
                
%                 delta(j).fstd=dat.fstd;
%                 delta(j).f=dat.f(tmp(3):tmp(6));
%                 delta(j).raw=dat.csc(dat.csc(tmp(3):tmp(6));
%       
               
                if abs(wavetype)==2 
                    delta(j).ind_peak2=tmp(5);
                    delta(j).ts_peak2=dat.ts(tmp(5));
                end
                               
              case 3
                               
                delta(j).type=wavetype;
                delta(j).amplitude=amp;
                delta(j).freq=0.5/dtm;
                delta(j).length=dat.ts(episode_end)- dat.ts(episode_start);
                
                
                delta(j).ampall=ampall;
                delta(j).ampmean=ampm;
                delta(j).dtall=dtall;
                delta(j).dtmean=dtm;
                delta(j).posneg=episode_posneg;
                
                delta(j).ind_threshs=episode_thresh;
                delta(j).ind_peaks=episode_allpeaks;
                
                delta(j).indstart=episode_start;
                delta(j).indstop=episode_end;
                delta(j).indmax=episode_mmax;
                delta(j).indmin=episode_mmin;
                delta(j).indmaxpeak=km(mma);
                delta(j).indmaxpeakpn=km(mma);
                
                delta(j).tsthreshs=dat.ts(episode_thresh);
                delta(j).tspeaks=dat.ts(episode_allpeaks);
                
                delta(j).tsstart=dat.ts(episode_start);
                delta(j).tsstop=dat.ts(episode_end);
                delta(j).tsabsmax=dat.ts(episode_mmax);
                delta(j).tsabsmin=dat.ts(episode_mmin);
                               
%                 delta(j).fstd=dat.fstd;
%                 delta(j).f=dat.f(episode_start:episode_end);
%                 delta(j).raw=dat.csc(episode_start:episode_end);  
                
        end    
        
        if isfield(dat,'scoring')
             k= knnsearch(dat.scoring.epochlimits', delta(j).tsabsmax - (dat.scoring.epoch/2) );
             delta(j).stage = dat.scoring.stage{k};
        end   
                
   
    
        if opt.verbose 
             
            disp([  '     amplitude criteria fulfilled! '] ); 
            %disp (['crossing ' num2str([ i  allind(i)  ] ) ]);
            hold on 
            plot(dat.ts(delta(j).indstart:delta(j).indstop),dat.env(delta(j).indstart:delta(j).indstop),'g');

            hold off
            axis ([dat.ts(delta(j).indstart)-win1/dat.fs dat.ts(delta(j).indstop)+win1/dat.fs -10 20])
            fprintf ( '     final %g %g \n  - \n' ,j, dat.ts(delta(j).indstart)) 
            pause
        end
        
          
       
        j=j+1;
        %detect=1;
    else
      
        if opt.verbose && ( tmp(1)-win0>0 )
            
            %(amp>=opt.minamp) (amp<opt.maxamp) ...
            % (len>=opt.minlen) (len<=opt.maxlen) ( tmp(1)-win0>0 )...
            %      ~any( ismember( tmp , [opt.noise.ind]))
            
            fprintf(  '   sw > criteria not fulfilled! \n' ); 
            fprintf(  '        > >=minamp  <=maxmap \n');
            fprintf(  '        >     %d       %d    \n',test_props(1:2))
          
            fprintf(  '        > >=minlen <=maxlen isnoise \n' )
            fprintf(  '        >    %d        %d      %d   \n', test_props (3:5))
            pause
        end
        %i=1+1;
   end
    


%     if ~opt.verbose
%         msg={''; ''; ['Checked ' num2str(i) ' of ' num2str(max_n)  ' threshold crossings!'] ; ' Press Ctrl-C to interrupt.'};
%         waitbar(i/max_n,mh,msg)
%         %progressbar(i/(max_n+20))
%     end
    if ~(opt.verbose || opt.verbosePlus)
        %mh=waitbar(i/max_n,mh,['Checked ' num2str(i) ' of ' num2str(max_n) ' threshold crossings!  Press Ctrl-C to interrupt.']);
        progressbar(i/(max_n+20),2)

    end
    
end

disp('') 

if ~opt.verbose
%     msg={''; ''; ['Detected ' num2str(length(delta)) ' delta waves out of ' num2str(max_n) ' threshold crossings!'] };
%     waitbar(1,mh,msg)
%     %progressbar(1)
%     pause(0.5)
%     delete (mh)
    
    
    
    progressbar(1,2)
    pause(1)
    close all hidden
    
end     
