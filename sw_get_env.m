function [dat,opt ]= sw_get_env(dat,opt)
% [env ]= detect_get_env(dat,opt)
%--------------------------------------------------------------------------
% function to filter and calculate the envelope (~power) from signal
% 
% inputs:  1) structure with fields
%             dat.csc  :   continuoulsy sampled channel data (signal)
%             dat.ts   :   time stamps for each csc sample
%             dat.fs   :   sampling frequency
%             (optional)
%             dat.info :   meta info on data

%          2) structure opt with fields:
%              opt.filterband = [ 0.5 4]; 
              
%              opt.smoothenv =0; 

% check opt fields: antype, refs

% prepare data
warning off all 

if isfield(opt,'pat')
    
     if isfield (dat, 'ch_list')
            fn0= [opt.pat opt.fn  '_' dat.ch_list{1} '_' opt.detect '_bandpass.mat']; 
    
     else
            fn0= [opt.pat opt.fn  '_' opt.detect '_bandpass.mat']; 
     end
    
else
    fn0='';
end

msg0=['Getting envelope...'] ;
mh = waitbar(0,msg0,'name', [opt.detect ' detection 0.2 beta']);

% load saved rectified data?
if opt.load && exist(fn0,'file') ==2 && ~strcmp(opt.ext,'.edf')
    warning ( 'Caution: Loading previously filtered data!')
    msg0=['Loading previously saved envelope from: ' fn0] ;
    waitbar(0.5,mh,msg0,'name', [opt.detect ' detection 0.2 beta']);
    load (fn0,'dat');
    waitbar(1,mh,'Done');
else
    msg0='Calculating envelope from raw data...';

    %% zscore raw signal for displaying only
    waitbar(2/4,mh,msg0); drawnow;
    
    % dat.csc([opt.noise.ind])=NaN;
    [dat.z, dat.std]=zscore(dat.csc,0,2); 

%         plot(dat.ts,dat.csc)
%         hold on
%         plot(dat.ts([opt.noise.ind]), dat.csc([opt.noise.ind]),'.r');
%         hold off
      
    %% filter
    waitbar(3/4,mh,msg0) ;drawnow;
    %sw_zscore(x,dim,mask,sigma)
    
    EEG.data=dat.csc;
    EEG.srate=dat.fs;
    EEG.trials=[];
    EEG.pnts=size(dat.csc,2);
    EEG.event=[];
    EEG.nbchan = size(dat.csc,1);
    
    [EEG] = pop_eegfiltnew(EEG, opt.filterband(1),opt.filterband(2)) ;
    
    %dat.f= eegfilt(dat.csc,dat.fs,opt.filterband(1),opt.filterband(2));%filter
    dat.f= EEG.data;
    clear EEG
    
    %dat.f= eegfilt(dat.csc,dat.fs,opt.filterband(1),opt.filterband(2))';%filter
    
    % zscore filtered signal for analysis
    dat.fz=[];
    for i=1:size(dat.f,1)
        
        [dat.fz(i,:), dat.fzmean(i), dat.fzstd(i)] =sw_zscore(dat.f(i,:),2,[opt.noise.ind]);
    end
     
    %% only for fast oscillations
    if opt.filterband(1)> 5 
       %  rectify
       [dat]=sw_rectify_csc(dat,0,[opt.noise.ind],[]);
      
        %spline fit peaks of zscored rectified signal: dat.rz
        [mx,ind] = findpeaks(dat.rz); 
        dat.env=spline(dat.ts(ind),mx,dat.ts); % spline envelope

        if opt.smoothenv(1)
            dat.env=moving_average(dat.env,opt.smoothenv(2)*cFs);
        end
        
         %%% or get envelope via hilbert transform
         % hilbert ransform
         % hh=abs(hilbert(cscx.z));
        
    else
        % direct peak detection for slow oscillations
        
        dat.env=dat.fz;
    
    end
    waitbar(4/4,mh,msg0); drawnow;
        
    if opt.save
        
        save (fn0, 'dat', 'opt' );
        waitbar(1,mh,['Saved filtered data to ' fn0 ]);
        drawnow;
    end

    
    
end


close(mh)





end

